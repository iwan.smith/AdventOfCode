#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>

#include "catch2/catch.hpp"
#include "fmt/format.h"

uint64_t day17part1(std::basic_istream<char>& input){
    std::string line; std::getline(input, line);
    int x1, x2, y1, y2;
    sscanf(line.c_str(), "target area: x=%d..%d, y=%d..%d", &x1, &x2, &y1, &y2);

    assert(y1 < 0);

    return (-y1)*(-y1 -1)/2;
}


uint64_t day17part2(std::basic_istream<char>& input){
    std::string line; std::getline(input, line);
    int x1, x2, y1, y2;
    sscanf(line.c_str(), "target area: x=%d..%d, y=%d..%d", &x1, &x2, &y1, &y2);


    const int minxSpeed = std::ceil(-0.5 + std::sqrt(1.f+8.f*x1)/2.f);
    const int maxXSpeed = x2;
    const int maxySpeed = -y1-1;

    int nSpeed = 0;
    for( int xSpeed = minxSpeed; xSpeed <= maxXSpeed; ++xSpeed) {
        for (int ySpeed = y1; ySpeed <= maxySpeed; ++ySpeed) {
            int xs = xSpeed;
            int ys = ySpeed;

            for (int x{0}, y{0}; y >= y1 || ys > 0;) {
                x += xs;
                y += ys;

                xs = std::max(0, xs-1);
                --ys;

                if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                    ++nSpeed;
                    break;
                }
            }
        }
    }
    return nSpeed;
}



TEST_CASE( "Day 17 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{"target area: x=20..30, y=-10..-5"};
        CHECK(day17part1(test1) == 45);
    }


    SECTION("Day 17") {
        std::ifstream realData("../input_17.txt");
        fmt::print("Day 17, Part 1: {}\n", day17part1(realData));
    }

}

TEST_CASE( "Day 17 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{"target area: x=20..30, y=-10..-5"};
        CHECK(day17part2(test1) == 112);
    }

    SECTION("Day 17") {
        std::ifstream realData("../input_17.txt");
        fmt::print("Day 17, Part 2: {}\n", day17part2(realData));
    }

}
