#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <unordered_set>
#include <queue>

#include "catch2/catch.hpp"
#include "fmt/format.h"
template<typename T>
using Cavern  = std::vector<std::vector<T>>;

struct Dijkstra {

    struct Node {
        size_t x, y;
        uint32_t distance;
    };

    const Cavern<uint32_t> &m_riskLevel;
    Cavern<uint32_t>        m_distances;
    Cavern<bool>            m_visited;

    std::priority_queue<Node, std::vector<Node>, decltype([](const Node &n1, const Node &n2) {
        return n1.distance > n2.distance;
    })> m_queueNodes;

    Dijkstra(const Cavern<uint32_t> &riskLevel)
            : m_riskLevel(riskLevel) {
        m_distances = riskLevel;
        for (size_t y = 0; y < riskLevel.size(); ++y) {
            m_visited.emplace_back();
            for (size_t x = 0; x < riskLevel.front().size(); ++x) {
                m_distances[y][x] = std::numeric_limits<uint32_t>::max();
                m_visited.back().push_back(false);
            }
        }
        m_distances[0][0] = 0;

    }
public:
    void processNeighbour( uint64_t x, uint64_t y, uint64_t x2, uint64_t y2){
        if ( y2 < m_visited.size() && x2 < m_visited[y2].size() && !m_visited[y2][x2]) {
            m_distances[y2][x2] = std::min(m_distances[y2][x2], m_distances[y][x] + m_riskLevel[y2][x2]);
            m_queueNodes.push({x2, y2, m_distances[y2][x2]});
        }

    }
    Cavern<uint32_t> run(uint64_t x, uint64_t y) {

        do {
            if (!m_visited[y][x]) {
                processNeighbour(x, y, x - 1, y);
                processNeighbour(x, y, x, y - 1);
                processNeighbour(x, y, x + 1, y);
                processNeighbour(x, y, x, y + 1);

                m_visited[y][x] = true;
            }

            auto minNode = m_queueNodes.top();
            x = minNode.x;
            y = minNode.y;

            m_queueNodes.pop();

        }while ( !m_queueNodes.empty() );

        return m_distances;
    }
};


uint64_t day15part1(std::basic_istream<char>& input){

    Cavern<uint32_t> riskLevel;

    for(std::string line; std::getline(input, line); ){
        riskLevel.emplace_back();
        std::transform(line.begin(), line.end(), std::back_inserter(riskLevel.back()), [](char c){return c-'0';});
    }

    const auto distances = Dijkstra(riskLevel).run(0, 0);

    return distances.back().back();


}


uint64_t day15part2(std::basic_istream<char>& input){

    Cavern<uint32_t> riskLevel;

    for(std::string line; std::getline(input, line); ){
        riskLevel.emplace_back();
        auto& arr = riskLevel.back();

        std::transform(line.begin(), line.end(), std::back_inserter(arr), [](char c){return c-'0';});
        size_t nX = arr.size();
        arr.resize(arr.size()*5);
        for ( int x = nX; x < arr.size(); ++x){
            arr[x] = arr[x-nX]%9+1;
        }
    }

    int nX = riskLevel.front().size();
    int nY = riskLevel.size();
    riskLevel.resize(nY*5);
    for ( int y = nY; y < riskLevel.size(); ++y){
        for( int x = 0; x < nX; ++x){
            riskLevel[y].push_back(riskLevel[y-nY][x]%9+1);
        }
    }

    const auto distances = Dijkstra(riskLevel).run(0, 0);

    return distances.back().back();
}



constexpr std::string_view smallTest1 = R"(1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581)";

TEST_CASE( "Day 15 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day15part1(test1) == 40);
    }


    SECTION("Day 15") {
        std::ifstream realData("../input_15.txt");
        fmt::print("Day 15, Part 1: {}\n", day15part1(realData));
    }

}

TEST_CASE( "Day 15 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day15part2(test1) == 315);
    }

    SECTION("Day 15") {
        std::ifstream realData("../input_15.txt");
        fmt::print("Day 15, Part 2: {}\n", day15part2(realData));
    }

}
