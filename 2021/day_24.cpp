#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>

#include "catch2/catch.hpp"

#include "fmt/format.h"


int solveDay24( std::array<int, 4> inputs, int nLoop, std::array<int64_t, 14>& modal ){

    std::array<int64_t, 14> modelNum{
            inputs[0]/1000,
            inputs[0]/100 %10,
            inputs[0]/10  %10,
            inputs[0]     %10 ,
            -1,
            -1,
            inputs[1],
            -1,
            -1,
            -1,
            inputs[2],
            -1,
            inputs[3],
            -1};

    for( const auto& i:modelNum)
        if ( i == 0 || i > 9)
            return -1;

    std::array<int64_t, 14> divisors{1 , 1 , 1 , 1 , 26, 26 ,1  ,26 ,26 ,26  ,1  ,26 ,1,  26 };
    std::array<int64_t, 14> adds    {11, 13, 11, 10, -3, -4 ,12 ,-8 ,-3 ,-12 ,14 ,-6 ,11, -12};
    std::array<int64_t, 14> adds2   {14, 8 , 4 , 10, 14, 10 ,4  ,14 ,1  ,6   ,0  ,9  ,13, 12 };
    int64_t x = 0;
    int64_t y = 0;
    int64_t z = 0;


    for( int i = 0; i < nLoop; ++i){

        int64_t& w = modelNum[i];

        x = z%26 + adds[i];
        z = z/divisors[i];

        if ( w == -1) {
            if (x >= 1 && x < 10)
                w = x;
            else
                return -1;
        }

        if ( x != w) {
            z = z * 26 + modelNum[i] + adds2[i];
        }

    }
    if ( z == 0){
        modal = modelNum;
    }
    return z;
}

TEST_CASE("Check Computer"){


};

int64_t day24part1(std::basic_istream<char>& input){

    std::array<int64_t, 14> modal{};

    auto arr2num = [&](){
        int64_t ret = 0;
        for( const auto&c: modal)
            ret = ret*10 + c;
        return ret;
    };

    for( int i = 9999; i > 0; --i ){
        if (solveDay24({i, 9, 9, 9}, 6, modal) == -1)
            continue;
        for( int j = 9; j > 0; --j ) {
            if (solveDay24({i, j, 9, 9}, 10, modal) == -1)
                continue;
            for( int k = 9; k > 0; --k ) {
                if (solveDay24({i, j, k, 9}, 12, modal) == -1)
                    continue;
                for (int l = 9; l > 0; --l) {
                    if (solveDay24({i, j, k, l}, 14, modal) == 0)
                        return arr2num();
                }
            }
        }
    }
    return -1;

}

int64_t day24part2(std::basic_istream<char>& input){

    std::array<int64_t, 14> modal{};

    auto arr2num = [&](){
        int64_t ret = 0;
        for( const auto&c: modal)
            ret = ret*10 + c;
        return ret;
    };

    for (int i = 1111; i <= 9999; ++i) {
        if (solveDay24({i, 9, 9, 9}, 6, modal) == -1)
            continue;
        for( int j = 1; j <= 9; ++j ) {
            if (solveDay24({i, j, 9, 9}, 10, modal) == -1)
                continue;
            for( int k = 1; k <= 9; ++k ) {
                if (solveDay24({i, j, k, 9}, 12, modal) == -1)
                    continue;
                for( int l = 1; l <= 9; ++l ){
                    if (solveDay24({i, j, k, l}, 14, modal) == 0)
                        return arr2num();
                }
            }
        }
    }
    return -1;

}

TEST_CASE( "Day 24 Part 1", "AOC" ) {
    SECTION("Day 24") {
        std::ifstream realData("../input_24.txt");
        fmt::print("Day 24, Part 1: {}\n", day24part1(realData));
    }
}

TEST_CASE( "Day 24 Part 2", "AOC" ) {
    SECTION("Day 24") {
        std::ifstream realData("../input_24.txt");
        fmt::print("Day 24, Part 2: {}\n", day24part2(realData));
    }
}
