#include <sstream>
#include <fstream>
#include <set>
#include <optional>

#include "catch2/catch.hpp"
#include "fmt/format.h"

int day08part1(std::basic_istream<char>& inputDisplay){

    int n1478 = 0;
    for( std::string line; std::getline(inputDisplay, line); ){
        std::stringstream ss(line);
        for( std::string word; ss >> word; ){
            if ( word == "|") break;
        }
        for( std::string word; ss >> word; ){
            if ( word.size() == 2 || word.size() == 4 || word.size() == 3 || word.size() == 7)
                ++n1478;
        }
    }
    return n1478;
}

uint8_t charToBinary(char num){
    return 1 << (num-'a');
}

uint8_t charToBinary(const std::string& num){
    uint8_t ret = 0;
    for( const auto& c: num)
        ret += charToBinary(c);
    return ret;
}

int day08part2(std::basic_istream<char>& inputDisplay){

    int ret = 0;

    for( std::string line; std::getline(inputDisplay, line); ){

        std::array<uint8_t, 10> numMap{};

        std::vector<uint8_t> possible069, possible235;

        std::stringstream ss(line);
        for( std::string word; ss >> word; ){
            if ( word == "|") break;
            if ( word.size() == 2) numMap[1] = charToBinary(word);
            if ( word.size() == 4) numMap[4] = charToBinary(word);
            if ( word.size() == 3) numMap[7] = charToBinary(word);
            if ( word.size() == 7) numMap[8] = charToBinary(word);
            if ( word.size() == 6) possible069.push_back(charToBinary(word));
            if ( word.size() == 5) possible235.push_back(charToBinary(word));
        }

        // We know 1 4 7 8 with certainty

        assert(possible069.size() == 3);
        assert(possible235.size() == 3);

        std::vector<uint8_t> p06;
        for( const auto& p069: possible069) {
            if ((p069 & numMap[4]) == numMap[4]) numMap[9] = p069;
            else p06.push_back(p069);
        }
        // We know 1 4 7 8 9 with certainty

        std::vector<uint8_t> p25;
        for( const auto& p235: possible235) {
            if ((p235 & numMap[7]) == numMap[7]) numMap[3] = p235;
            else p25.push_back(p235);
        }
        // We know 1 3 4 7 8 9 with certainty

        assert(p06.size() == 2);
        assert(p25.size() == 2);

        auto e = numMap[9]^numMap[8];
        numMap[2] = (p25[0] & e) ? p25[0] : p25[1];
        numMap[5] = (p25[0] & e) ? p25[1] : p25[0];

        // We know 1 2 3 4 5 7 8 9 with certainty

        numMap[0] = ((p06[0] & numMap[1]) == numMap[1])  ? p06[0] : p06[1];
        numMap[6] = ((p06[0] & numMap[1]) == numMap[1])  ? p06[1] : p06[0];

        // We know 0 1 2 3 4 5 6 7 8 9 with certainty

        int num = 0;
        for( std::string word; ss >> word; ){
            uint8_t binWord = charToBinary(word);
            int digit = std::ranges::find(numMap, binWord)-numMap.begin();
            num = num*10 + digit;
        }
        ret += num;
    }

    return ret;
}




constexpr std::string_view smallTest = R"(be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce)";

TEST_CASE( "Day 08 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day08part1(test1) == 26);
    }

    SECTION("Day 08") {
        std::ifstream realData("../input_08.txt");
        fmt::print("Day 08, Part 1: {}\n", day08part1(realData));
    }

}

TEST_CASE( "Day 08 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day08part2(test1) == 61229);
    }

    SECTION("Day 08") {
        std::ifstream realData("../input_08.txt");
        fmt::print("Day 08, Part 2: {}\n", day08part2(realData));
    }

}
