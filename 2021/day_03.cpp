#include <sstream>
#include <fstream>

#include "catch2/catch.hpp"
#include "fmt/format.h"

int day03part1(std::basic_istream<char>& diagnostics){
    std::vector<int> bits;

    for(std::string line; std::getline(diagnostics, line);){
       if (bits.empty()){
           bits.resize(line.size(), 0);
       }
       for(int i = 0; i < line.size(); ++i) {
           assert(line[i] == '0' or line[i] == '1' );
           bits[i] += (line[i] - '0') * 2 - 1;
       }
    }

    int gammaRate{0} ,epsilonRate{0};
    for( const auto& bit: bits){
        assert(bit != 0);
        gammaRate   = gammaRate*2   + (bit>0);
        epsilonRate = epsilonRate*2 + (bit<0);
    }
    return gammaRate * epsilonRate;
}

template<bool OxOrCO2>
std::vector<unsigned> processDiagnostics(const std::vector<unsigned>& input, unsigned bit){
    std::vector<unsigned> ret;
    int countBits = 0;

    for(const auto& diag: input){
        countBits += (diag&bit) ? 1 : -1;
    }

    for (const auto &diag: input) {
        if constexpr(OxOrCO2) {                     // If Oxygen
            if (countBits >= 0)                     // If '1' is more common
                if ((diag & bit) > 0)               // If diag has '1' at that position
                    ret.push_back(diag);
            if (countBits < 0)                      // If '0' is more common
                if ((diag & bit) == 0)              // If diag has '0' at that position
                    ret.push_back(diag);
        } else {                                    // If CO2
            if (countBits < 0)                      // If '1' is least common
                if ((diag & bit) > 0)               // If diag has '1' at that position
                    ret.push_back(diag);
            if (countBits >= 0)                     // If '0' is least common
                if ((diag & bit) == 0)              // If diag has '0' at that position
                    ret.push_back(diag);
        }
    }
    return ret;
}

std::pair<unsigned,unsigned> getOxCO2(const std::vector<unsigned>& input, unsigned bit){

    std::vector<unsigned> OxVals = input;
    for (unsigned tmpBit = bit; OxVals.size() > 1; tmpBit/=2){
        OxVals = processDiagnostics<true>(OxVals, tmpBit);
    }

    std::vector<unsigned> CO2Vals = input;
    for (unsigned tmpBit = bit; CO2Vals.size() > 1; tmpBit/=2){
        CO2Vals = processDiagnostics<false>(CO2Vals, tmpBit);
    }

    return {OxVals[0], CO2Vals[0]};

}

int day03part2(std::basic_istream<char>& diagnostics){
    std::vector<unsigned> diagnosticsVec;

    unsigned nBits = 0;
    for(std::string line; std::getline(diagnostics, line);){
        unsigned diagnostic{0};
        for(const auto& c: line){
            diagnostic = diagnostic*2 + (c=='1');
        }
        diagnosticsVec.push_back(diagnostic);
        nBits = line.size();
    }

    const auto [Ox,Co2] = getOxCO2(diagnosticsVec, 1<<(nBits-1));
    return Ox*Co2;

}


constexpr std::string_view smallTest = "00100\n"
                                       "11110\n"
                                       "10110\n"
                                       "10111\n"
                                       "10101\n"
                                       "01111\n"
                                       "00111\n"
                                       "11100\n"
                                       "10000\n"
                                       "11001\n"
                                       "00010\n"
                                       "01010\n";

TEST_CASE( "Day 03 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day03part1(test1) == 198);
    }

    SECTION("Day 03") {
        std::ifstream realData("../input_03.txt");
        fmt::print("Day 03, Part 1: {}\n", day03part1(realData));
    }

}

TEST_CASE( "Day 03 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day03part2(test1) == 230);
    }

    SECTION("Day 03") {
        std::ifstream realData("../input_03.txt");
        fmt::print("Day 03, Part 2: {}\n", day03part2(realData));
    }

}
