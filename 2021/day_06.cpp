#include <sstream>
#include <fstream>
#include <set>
#include <optional>

#include "catch2/catch.hpp"
#include "fmt/format.h"

int day06part1(std::basic_istream<char>& inputFish){

    std::array<uint32_t, 9> fishes{};

    char ignoreComma;
    for( uint32_t fish; inputFish >> fish; inputFish >> ignoreComma){
        ++fishes[fish];
    }

    for( int i = 0; i < 80; ++i){
        std::rotate(fishes.begin(), fishes.begin()+1, fishes.end());
        fishes[6] += fishes.back();
    }

    return std::accumulate(fishes.begin(), fishes.end(), 0);

}

uint64_t day06part2(std::basic_istream<char>& inputFish){

    std::array<uint64_t , 9> fishes{0,0,0,0,0,0,0,0,0};

    char ignoreComma;
    for( uint32_t fish; inputFish >> fish; inputFish >> ignoreComma){
        ++fishes[fish];
    }

    for( int i = 0; i < 256; ++i){
        std::rotate(fishes.begin(), fishes.begin()+1, fishes.end());
        fishes[6] += fishes.back();
    }

    return std::accumulate(fishes.begin(), fishes.end(), uint64_t(0));

}



constexpr std::string_view smallTest = R"(3,4,3,1,2)";

TEST_CASE( "Day 06 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day06part1(test1) == 5934);
    }

    SECTION("Day 06") {
        std::ifstream realData("../input_06.txt");
        fmt::print("Day 06, Part 1: {}\n", day06part1(realData));
    }

}

TEST_CASE( "Day 06 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day06part2(test1) == 26984457539);
    }

    SECTION("Day 06") {
        std::ifstream realData("../input_06.txt");
        fmt::print("Day 06, Part 2: {}\n", day06part2(realData));
    }

}
