#include <sstream>
#include <fstream>
#include <set>
#include <optional>

#include "catch2/catch.hpp"
#include "fmt/format.h"

std::vector<std::string> getHeights(std::basic_istream<char>& inputHeight){
    std::vector<std::string> heights(1);

    uint32_t mapWidth = 0;
    for (std::string line; std::getline(inputHeight, line);){
        heights.push_back(line);
        heights.back() = '9' + heights.back() + '9';
        mapWidth = heights.back().size();
    }

    heights.push_back(std::string(mapWidth, '9'));
    heights.front() = std::string(mapWidth, '9');

    return heights;

}


uint32_t day09part1(std::basic_istream<char>& inputHeight){

    uint32_t ret = 0;

    std::vector<std::string> heights = getHeights(inputHeight);

    for( int i = 1; i < heights.size()-1; ++i) {
        for (int j = 1; j < heights.front().size() - 1; ++j) {

            char height = heights[i][j];
            if (height < heights[i-1][j  ]
             && height < heights[i+1][j  ]
             && height < heights[i  ][j-1]
             && height < heights[i  ][j+1]){
                ret += 1+(height-'0');
            }
        }
    }

    return ret;
}


void getBasinSize( std::vector<std::string>& heights, uint32_t i, uint32_t j, uint32_t& bSize ){

    if ( heights[i][j] == '9' ) return;

    ++bSize;
    heights[i][j] = '9';
    getBasinSize(heights, i+1, j,   bSize);
    getBasinSize(heights, i-1, j,   bSize);
    getBasinSize(heights, i  , j+1, bSize);
    getBasinSize(heights, i  , j-1, bSize);

}


uint32_t day09part2(std::basic_istream<char>& inputHeight){

    std::vector<uint32_t> basins;

    std::vector<std::string> heights = getHeights(inputHeight);

    for( int i = 1; i < heights.size()-1; ++i) {
        for (int j = 1; j < heights.front().size() - 1; ++j) {
            uint32_t basinSize = 0;
            getBasinSize(heights, i, j, basinSize);
            if (basinSize > 0) {
                basins.push_back(basinSize);
            }
        }
    }

    std::partial_sort(basins.begin(), basins.begin()+3, basins.end(), std::greater<uint32_t>() );
    return basins[0]*basins[1]*basins[2];
}




constexpr std::string_view smallTest = R"(2199943210
3987894921
9856789892
8767896789
9899965678)";

TEST_CASE( "Day 09 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day09part1(test1) == 15);
    }

    SECTION("Day 09") {
        std::ifstream realData("../input_09.txt");
        fmt::print("Day 09, Part 1: {}\n", day09part1(realData));
    }

}

TEST_CASE( "Day 09 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day09part2(test1) == 1134);
    }

    SECTION("Day 09") {
        std::ifstream realData("../input_09.txt");
        fmt::print("Day 09, Part 2: {}\n", day09part2(realData));
    }

}
