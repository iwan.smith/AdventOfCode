#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>

#include "catch2/catch.hpp"
#include "fmt/format.h"



uint32_t day10part1(std::basic_istream<char>& inputChunks){
    const std::unordered_map<char, int> points{{')',3}, {']',57}, {'}', 1197}, {'>', 25137}};

    int score = 0;
    for (std::string line; std::getline(inputChunks, line);){
        std::string stack;

        for( const auto& c: line){
            if (std::string_view{"{[<("}.find(c) != std::string::npos )
                stack.push_back(c);
            else if (std::abs(c-stack.back()) <=2  ) //horrible ascii hacking
                stack.pop_back();
            else{
                score += points.at(c);
                break;
            }
        }
    }

    return score;
}


uint64_t day10part2(std::basic_istream<char>& inputChunks){
    const std::unordered_map<char, int> points{{'(',1}, {'[',2}, {'{', 3}, {'<', 4}};

    std::vector<uint64_t> scores;
    for (std::string line; std::getline(inputChunks, line);){
        std::string stack;

        bool corrupted = false;
        for( const auto& c: line){
            if (std::string_view{"{[<("}.find(c) != std::string::npos )
                stack.push_back(c);
            else if (std::abs(c-stack.back()) <=2  ) //horrible ascii hacking
                stack.pop_back();
            else{
                corrupted = true;
                break;
            }
        }
        uint64_t score = 0;
        if (!corrupted){
            for( const auto& c: std::ranges::reverse_view{stack})
                score = score*5 + points.at(c);
            scores.push_back(score);
        }
    }
    std::ranges::sort(scores);

    return scores[scores.size()/2];
}




constexpr std::string_view smallTest = R"([({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]])";

TEST_CASE( "Day 10 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day10part1(test1) == 26397);
    }

    SECTION("Day 10") {
        std::ifstream realData("../input_10.txt");
        fmt::print("Day 10, Part 1: {}\n", day10part1(realData));
    }

}

TEST_CASE( "Day 10 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day10part2(test1) == 288957);
    }

    SECTION("Day 10") {
        std::ifstream realData("../input_10.txt");
        fmt::print("Day 10, Part 2: {}\n", day10part2(realData));
    }

}
