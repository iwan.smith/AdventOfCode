#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>

#include "catch2/catch.hpp"
#include "fmt/format.h"

uint64_t day20(std::basic_istream<char>& input, int nEnhance){
    std::string enhanceAlgo; std::getline(input, enhanceAlgo);
    assert(enhanceAlgo.size() == 512);

    auto border = nEnhance*2+1;

    std::vector<std::vector<int>> image(border);

    //empty line
    std::string line; std::getline(input, line);

    while (std::getline(input, line)){
        std::vector<int> lineData (border, 0);
        for( const auto c: line){
            lineData.push_back(c == '#');
        }
        lineData.resize(lineData.size() + border, 0);
        image.emplace_back(std::move(lineData));
    }
    auto width = image.back().size();
    for( int i = 0; i < border; ++i)
        image[i] = std::vector<int>(width, 0);
    image.resize(image.size()+border,   std::vector<int>(width, 0));

    auto getPixel = [&](int x, int y)->int{
        if ( x < 0 || x >= width || y < 0 || y >= image.size() ){
            return image.front().front();
        }
        return image[y][x];
    };

    for ( int enhance = 0; enhance < nEnhance; ++ enhance){
        auto newImage = image;
        for (int y = 0; y < image.size(); ++y) {
            for (int x = 0; x < width; ++x) {
                unsigned address = 0;
                for (int i: {-1, 0, +1}) {
                    for (int j: {-1, 0, +1}) {
                        address = address * 2 + getPixel(x + j, y + i);
                    }
                }
                assert(address < 512);
                newImage[y][x] = enhanceAlgo[address] == '#' ? 1 : 0;
            }
        }
        image = std::move(newImage);

    }


    int lightPixels = 0;
    for( const auto& imLine: image){
        for( const auto& pix: imLine){
            lightPixels += pix;
        }
    }
    return lightPixels;
}


uint64_t day20part2(std::basic_istream<char>& input){
    return 0;
}

constexpr std::string_view smallTestInput = R"(..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###)";

TEST_CASE( "Day 20 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTestInput)};
        CHECK(day20(test1, 2) == 35);
    }


    SECTION("Day 20") {
        std::ifstream realData("../input_20.txt");
        fmt::print("Day 20, Part 1: {}\n", day20(realData, 2));
    }

}

TEST_CASE( "Day 20 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTestInput)};
        CHECK(day20(test1, 50) == 3351);
    }


    SECTION("Day 20") {
        std::ifstream realData("../input_20.txt");
        fmt::print("Day 20, Part 2: {}\n", day20(realData, 50));
    }

}
