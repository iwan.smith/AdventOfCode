#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>
#include <cstdlib>

#include "catch2/catch.hpp"

#include "fmt/format.h"


namespace {

    struct Burrow{
        std::array<char, 11> hall{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'};
        std::array<std::array<char, 4>, 4> rooms;
        int64_t score{0};
    };

    Burrow getInput(std::basic_istream<char> &input) {
        Burrow ret;
        std::string line;
        std::getline(input, line); // "#############"
        std::getline(input, line); // "#...........#"
        std::getline(input, line); // "###B#A#B#C###"

        ret.rooms[0][0] = line[3];
        ret.rooms[1][0] = line[5];
        ret.rooms[2][0] = line[7];
        ret.rooms[3][0] = line[9];

        ret.rooms[0][1] = 'D';
        ret.rooms[1][1] = 'C';
        ret.rooms[2][1] = 'B';
        ret.rooms[3][1] = 'A';

        ret.rooms[0][2] = 'D';
        ret.rooms[1][2] = 'B';
        ret.rooms[2][2] = 'A';
        ret.rooms[3][2] = 'C';

        std::getline(input, line); // "  #D#A#D#C#"

        ret.rooms[0][3] = line[3];
        ret.rooms[1][3] = line[5];
        ret.rooms[2][3] = line[7];
        ret.rooms[3][3] = line[9];

        return ret;
    }


    std::vector<Burrow> moveAmphipodFromBurrow(const Burrow &b, char col) {
        std::vector<Burrow> ret;

        auto& room = b.rooms[col-'A'];

        size_t col_it = (col-'A')*2 + 2;

        // find top most amphipod
        size_t i = 0;
        while (room[i] == '.' && i < 4)
            ++i;

        // if no amphipods, return nothing
        if (i == 4)
            return {};

        // if all amphipods are in correct room, return nothing
        if ( std::count(room.begin() + i, room.end(), col) == 4-i)
            return {};

        for (const auto &dx: {-1, 1}) {
            for (size_t j = col_it; j < 11; j += dx) {
                if (b.hall[j] != '.')
                    break;
                if (j == 2 || j == 4 || j == 6 || j == 8)
                    continue;

                auto newBurrow = b;

                newBurrow.hall[j] = newBurrow.rooms[col-'A'][i];
                newBurrow.rooms[col-'A'][i] = '.';
                int dcol = std::abs(int(col_it) - int(j));
                int drow = i + 1;
                int cpm = std::pow(10, newBurrow.hall[j] - 'A');
                int64_t cost = (dcol + drow) * cpm;

                newBurrow.score += cost;

                ret.emplace_back(newBurrow);

            }

        }

        return ret;

    }
    std::optional<Burrow> moveAmphipodIntoBurrow(const Burrow &b, const size_t col_it) {

        if (b.hall[col_it] == '.')
            return {};

        int destCol = 2*( b.hall[col_it]-'A' ) + 2;
        int dx = (int(destCol)-int(col_it))/std::abs(int(destCol)-int(col_it));

        for( int i = col_it+dx; i != destCol; i += dx){
            if ( b.hall[i] != '.')
                return {};
        }

        auto newBurrow = b;

        auto& room = newBurrow.rooms[b.hall[col_it]-'A'];
        int j = 3;
        for( ; j > 0; --j ){
            if ( room[j] == '.' )
                break;
            if ( room[j] != b.hall[col_it])
                return {};
        }

        room[j] = newBurrow.hall[col_it];
        newBurrow.hall[col_it] = '.';

        int dcol = std::abs(int(col_it)-int(destCol));
        int drow = j+1;
        int cpm = std::pow(10, room[j] - 'A');

        newBurrow.score += (dcol+drow)*cpm;

        return newBurrow;

    }


        std::vector<Burrow> getAllMoves(const Burrow &b) {
        std::vector<Burrow> ret;

        auto As = moveAmphipodFromBurrow(b, 'A');
        ret.insert(ret.end(), As.begin(), As.end());

        auto Bs = moveAmphipodFromBurrow(b, 'B');
        ret.insert(ret.end(), Bs.begin(), Bs.end());

        auto Cs = moveAmphipodFromBurrow(b, 'C');
        ret.insert(ret.end(), Cs.begin(), Cs.end());

        auto Ds = moveAmphipodFromBurrow(b, 'D');
        ret.insert(ret.end(), Ds.begin(), Ds.end());

        for( int i = 0; i <11; ++i){
            auto moved = moveAmphipodIntoBurrow(b, i);
            if (moved.has_value())
                ret.emplace_back(std::move(moved.value()));
        }
        return ret;
    }

    bool isFinished(const Burrow& b){
        return std::ranges::count(b.hall, '.') == 11
                && std::ranges::count(b.rooms[0], 'A') == 4
                && std::ranges::count(b.rooms[1], 'B') == 4
                && std::ranges::count(b.rooms[2], 'C') == 4
                && std::ranges::count(b.rooms[3], 'D') == 4;
    }

    int64_t getCheapestCost(const Burrow& b){
        int64_t ret = std::numeric_limits<int64_t>::max();

        for( const auto& move: getAllMoves(b)){
            if ( isFinished(move) ){
                ret = std::min(ret, move.score);
            }
            ret = std::min(ret, getCheapestCost(move));
        }
        return ret;
    }


}



uint64_t day23part1(std::basic_istream<char>& input){
    Burrow start = getInput(input);
    return getCheapestCost(start);

}
uint64_t day23part2(std::basic_istream<char>& input){
    return 0;
}


TEST_CASE( "Day 23 Part 1", "AOC" ) {


    SECTION("Day 23") {
        std::ifstream realData("../input_23.txt");
        fmt::print("Day 23, Part 1: {}\n", day23part1(realData));
    }

}

TEST_CASE( "Day 23 Part 2", "AOC" ) {


    SECTION("Day 23") {
        std::ifstream realData("../input_23.txt");
        fmt::print("Day 23, Part 2: {}\n", day23part2(realData));
    }

}
