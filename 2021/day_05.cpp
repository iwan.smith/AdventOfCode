#include <sstream>
#include <fstream>
#include <set>
#include <optional>

#include "catch2/catch.hpp"
#include "fmt/format.h"

void processLine(int32_t x0, int32_t y0, int32_t x1, int32_t y1, std::unordered_map<int64_t, int>& ventCount) {
    int32_t dx = (x1 - x0) / std::max(1, std::abs(x1 - x0));
    int32_t dy = (y1 - y0) / std::max(1, std::abs(y1 - y0));

    for (;; x0 += dx, y0 += dy) {
        int64_t coord = (static_cast<int64_t>(x0) << 32) + y0;
        ++ventCount[coord];

        if ( x0 == x1 && y0==y1) break;
    }
}

int day05part1(std::basic_istream<char>& ventsStream){

    std::unordered_map<int64_t, int> ventCount;
    for( std::string vent; std::getline(ventsStream, vent); ) {
        int32_t x0{0}, y0{0}, x1{0}, y1{0};
        sscanf(vent.c_str(), "%d,%d -> %d,%d", &x0, &y0, &x1, &y1);

        if ( !(x0 == x1) && ! (y0 == y1))
            continue;
        processLine(x0, y0, x1, y1, ventCount);
    }

    return std::ranges::count_if(ventCount, [](auto& itr){ return itr.second>1;});
}

int day05part2(std::basic_istream<char>& ventsStream){

    std::unordered_map<int64_t, int> ventCount;
    for( std::string vent; std::getline(ventsStream, vent); ) {
        int32_t x0{0}, y0{0}, x1{0}, y1{0};
        sscanf(vent.c_str(), "%d,%d -> %d,%d", &x0, &y0, &x1, &y1);

        processLine(x0, y0, x1, y1, ventCount);
    }

    return std::ranges::count_if(ventCount, [](auto& itr){ return itr.second>1;});
}



constexpr std::string_view smallTest = R"(0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2)";

TEST_CASE( "Day 05 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day05part1(test1) == 5);
    }

    SECTION("Day 05") {
        std::ifstream realData("../input_05.txt");
        fmt::print("Day 05, Part 1: {}\n", day05part1(realData));
    }

}

TEST_CASE( "Day 05 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day05part2(test1) == 12);
    }

    SECTION("Day 05") {
        std::ifstream realData("../input_05.txt");
        fmt::print("Day 05, Part 2: {}\n", day05part2(realData));
    }

}
