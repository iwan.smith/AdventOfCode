#include <sstream>
#include <fstream>

#include "catch2/catch.hpp"
#include "fmt/format.h"

int day02part1(std::basic_istream<char>& commands){

    int depth{0}, horiz{0};
    for( std::string line; std::getline(commands, line); ){
        std::array<char, 8> direction{};
        int distance;

        sscanf(line.c_str(), "%s %d", direction.data(), &distance);

        const std::string_view dir{direction.data()};
        if ( dir == "forward"){
            horiz += distance;
        } else if (dir == "down"){
            depth += distance;
        } else if (dir == "up"){
            depth -= distance;
        } else {
            assert(false);
        }
    }
    return depth * horiz;
}

int day02part2(std::basic_istream<char>& commands){

    int depth{0}, horiz{0}, aim{0};
    for( std::string line; std::getline(commands, line); ){
        std::array<char, 8> direction;
        int distance;

        sscanf(line.c_str(), "%s %d", direction.data(), &distance);

        const std::string_view dir{direction.data()};
        if ( dir == "forward"){
            horiz += distance;
            depth += aim*distance;
        } else if (dir == "down"){
            aim += distance;
        } else if (dir == "up"){
            aim -= distance;
        } else {
            assert(false);
        }
    }
    return depth * horiz;
}


constexpr std::string_view smallTest = "forward 5\n"
                                       "down 5\n"
                                       "forward 8\n"
                                       "up 3\n"
                                       "down 8\n"
                                       "forward 2\n";

TEST_CASE( "Day 02 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day02part1(test1) == 150);
    }

    SECTION("Day 02") {
        std::ifstream realData("../input_02.txt");
        fmt::print("Day 02, Part 1: {}\n", day02part1(realData));
    }

}

TEST_CASE( "Day 02 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day02part2(test1) == 900);
    }

    SECTION("Day 02") {
        std::ifstream realData("../input_02.txt");
        fmt::print("Day 02, Part 2: {}\n", day02part2(realData));
    }

}
