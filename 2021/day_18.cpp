#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>

#include "catch2/catch.hpp"
#include "fmt/format.h"

namespace {
    using Pair = std::array<int, 16>;
    using DoublePair = std::array<int, 32>;

    constexpr Pair startingNum{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
    constexpr DoublePair startingDoubleNum{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

    template<size_t N>
    void Parse(std::string_view line, std::array<int, N>& num, size_t loc = 0, size_t stride = N/2) {
        if (line[0] != '[') {
            num[loc] = std::stoi(std::string(line));
            return;
        }

        int countSquare = 0;
        std::string_view leftRight{line.begin() + 1, line.end() - 1};

        size_t commasep{0};
        for (int i = 0; i < leftRight.size(); ++i) {
            if (leftRight[i] == '[') ++countSquare;
            if (leftRight[i] == ']') --countSquare;
            if (leftRight[i] == ',' && countSquare == 0) {
                commasep = i;
                break;
            }
        }
        std::string_view left{leftRight.begin(), leftRight.begin() + commasep};
        std::string_view right{leftRight.begin() + commasep + 1, leftRight.end()};

        Parse(left,  num, loc,            stride/2);
        Parse(right, num, loc+stride, stride/2);
    }

    template<size_t N>
    std::string PairToString(const std::array<int, N>& p, const int loc = 0, const int stride = N/2) {
        if ( stride == 0 || p[loc+stride] == -1)
            return fmt::format("{}", p[loc]);

        auto left  = PairToString(p, loc,        stride/2);
        auto right = PairToString(p, loc+stride, stride/2);
        return fmt::format("[{},{}]", left, right);
    }

    void Explode( DoublePair& pair){

        for( int loc = 0; loc < pair.size(); loc += 2) {

            if ( pair[loc+1] == -1) continue;

            auto left  = pair[loc];
            auto right = pair[loc+1];

            pair[loc] = 0;
            pair[loc+1] = -1;

            for (int i = loc - 1; i >= 0; --i) {
                if (pair[i] != -1) {
                    pair[i] += left;
                    break;
                }
            }
            for (int i = loc + 1; i < pair.size(); ++i) {
                if (pair[i] != -1) {
                    pair[i] += right;
                    break;
                }
            }
        }

    }

    // return true if a split has occurred
    bool split(DoublePair& pair, size_t loc = 0, size_t stride = 16){
        if (stride == 0) return false;
        if (pair[loc] == -1 ) return false;

        if (auto val = pair[loc]; val >= 10 ){

            while ( pair[loc+stride] >= 0) stride /= 2;

            pair[loc]        = val/2;
            pair[loc+stride] = val/2 + val%2;

            return true;
        }

        if ( split(pair, loc, stride/2) )
            return true;
        return split(pair, loc+stride, stride/2);

    }

    void reduce(DoublePair& pair){
        auto finished = [&](){
            for( int i = 1; i < pair.size(); i+=2)
                if (pair[i] >= 0)
                    return false;
            for( int i = 0; i < pair.size(); ++i)
                if (pair[i] >= 10)
                    return false;
            return true;
        };
        while (!finished()){
            Explode(pair);
            split(pair);
        }
    }

    Pair add( const Pair& pair1, const Pair& pair2){
        DoublePair sum;
        std::ranges::copy(pair1, sum.begin());
        std::ranges::copy(pair2, sum.begin()+16);

        reduce (sum);
        Pair ret;
        for( int i = 0; i < sum.size(); i += 2)
            ret[i/2] = sum[i];
        return ret;
    }

    Pair sumAll( std::vector<Pair>& nums){
        auto sum = add(std::move(nums[0]), std::move(nums[1]));
        for( int i = 2; i < nums.size(); ++i ){
            sum = add(std::move(sum), std::move(nums[i]));
        }
        return sum;
    }

    int64_t magnitude(const Pair& pair, int loc = 0, int stride = 8){
        if ( stride == 0) return pair[loc];

        if ( pair[loc+stride] == -1 ) return pair[loc];

        return 3*magnitude(pair, loc, stride/2) + 2* magnitude(pair, loc+stride, stride/2);

    }


    }// end anon namespace;


int64_t day18part1(std::basic_istream<char> &input) {

    std::vector<Pair> nums;
    for( std::string line; std::getline(input, line); ){
        Pair snailNum = startingNum;
        Parse(line, snailNum);
        nums.emplace_back(snailNum);
    }
    auto sum = sumAll(nums);

    return magnitude(sum);
}

uint64_t day18part2(std::basic_istream<char>& input){
    std::vector<Pair> nums;
    for( std::string line; std::getline(input, line); ){
        Pair snailNum = startingNum;
        Parse(line, snailNum);
        nums.emplace_back(snailNum);
    }

    int64_t maxSum = 0;
    for( int i = 0; i < nums.size(); ++i) {
        for (int j = 0; j < nums.size(); ++j) {
            if ( i == j) continue;

            auto sum = add(nums[i],  nums[j]);

            maxSum = std::max(maxSum, magnitude(sum));
        }
    }


    return maxSum;


}


TEST_CASE( "Day 18 Test Parsing", "AOC" ) {

    auto snailNumStr = GENERATE(
        "[1,2]",
        "[[1,2],3]",
        "[9,[8,7]]",
        "[[1,9],[8,5]]",
        "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]",
        "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]",
        "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]"
    );

    Pair snailNum = startingNum;
    Parse(snailNumStr, snailNum);

    CHECK(PairToString(snailNum) == snailNumStr);


}

TEST_CASE( "Day 18 Test Exploding", "AOC" ) {

    const std::vector<std::pair<std::string_view, std::string_view>> explosions{
            {"[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"},
            {"[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"},
            {"[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]"},
            {"[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"},

    };
    for( const auto& [before, after]: explosions ) {
        DoublePair snailNum = startingDoubleNum;

        Parse(before, snailNum);
        Explode(snailNum);
        CHECK(PairToString(snailNum) == after);
    }
}


TEST_CASE( "Day 18 Test Reducing", "AOC" ) {

    const std::vector<std::pair<std::string_view, std::string_view>> explosions{
            {"[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"},

    };
    for( const auto& [before, after]: explosions ) {
        DoublePair snailNum = startingDoubleNum;
        Parse(before, snailNum);
        reduce(snailNum);
        CHECK(PairToString(snailNum) == after);
    }

}


std::string_view smallTestStr1 = R"([1,1]
[2,2]
[3,3]
[4,4])";

std::string_view smallTestStr2 = R"([1,1]
[2,2]
[3,3]
[4,4]
[5,5])";

std::string_view smallTestStr3 = R"([1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6])";

std::string_view smallTestStr4 = R"([[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]])";

TEST_CASE( "Day 18 Test Summing", "AOC" ) {

    const std::vector<std::pair<std::string_view, std::string_view>> sums{
            {smallTestStr1, "[[[[1,1],[2,2]],[3,3]],[4,4]]"},
            {smallTestStr2, "[[[[3,0],[5,3]],[4,4]],[5,5]]"},
            {smallTestStr3, "[[[[5,0],[7,4]],[5,5]],[6,6]]"},
            {smallTestStr4, "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"},
    };

    for( const auto& [before, after]: sums ) {
        std::vector<Pair> nums;
        std::stringstream  input{std::string(before)};
        for( std::string line; std::getline(input, line); ){
            Pair snailNum = startingNum;
            Parse(line, snailNum);
            nums.emplace_back(snailNum);
        }

        auto sum = sumAll(nums);

        CHECK(PairToString(sum) == after);
    }
}

TEST_CASE( "Day 18 Test Magnitude", "AOC" ) {

    const std::vector<std::pair<std::string_view, int>> reductions{
            {"[[1,2],[[3,4],5]]", 143},
            {"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384},
            {"[[[[1,1],[2,2]],[3,3]],[4,4]]", 445},
            {"[[[[3,0],[5,3]],[4,4]],[5,5]]", 791},
            {"[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137},
            {"[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488},
    };
    for( const auto& [before, after]: reductions ) {
        Pair snailNum = startingNum;
        Parse(before, snailNum);
        CHECK(magnitude(snailNum) == after);
    }
}

constexpr std::string_view homework1 = R"([[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]])";

TEST_CASE( "Day 18 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(homework1) };
        CHECK(day18part1(test1) == 4140);
    }

    SECTION("Day 18") {
        std::ifstream realData("../input_18.txt");
        fmt::print("Day 18, Part 1: {}\n", day18part1(realData));
    }

}

TEST_CASE( "Day 18 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(homework1) };
        CHECK(day18part2(test1) == 3993);
    }

    SECTION("Day 18") {
        std::ifstream realData("../input_18.txt");
        fmt::print("Day 18, Part 2: {}\n", day18part2(realData));
    }

}
