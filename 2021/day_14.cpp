#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <unordered_set>

#include "catch2/catch.hpp"
#include "fmt/format.h"


auto getPolymerInsertions(std::basic_istream<char>& input){
    std::string polymer; std::getline(input, polymer);

    {
        std::string emptyLine; std::getline(input, emptyLine);
    }

    std::unordered_map<std::string, char> insertions;
    for( std::string line; std::getline(input, line);){
        std::string polPair(2, '\0');
        char polInsert;
        sscanf(line.c_str(), "%2c -> %1c", polPair.data(), &polInsert);
        insertions[polPair] = polInsert;
    }

    return std::pair(polymer, insertions);

}



uint64_t day14part1(std::basic_istream<char>& input){

    auto [polymer, insertions] = getPolymerInsertions(input);

    for( int it = 0; it < 10; ++it) {
        std::string newPolymer;
        for (int i = 0; i < polymer.size() - 1; ++i) {
            std::string pairPol(polymer.c_str() + i, 2);

            newPolymer.push_back(polymer[i]);
            newPolymer.push_back(insertions.at(pairPol));
        }
        newPolymer.push_back(polymer.back());
        polymer = std::move(newPolymer);
    }

    std::unordered_map<char, size_t> countElems;
    for(const auto& c: polymer){
        ++countElems[c];
    }
    size_t min = std::numeric_limits<size_t>::max();
    size_t max = 0;
    for( const auto& [_, count]: countElems){
        min = std::min(count, min);
        max = std::max(count, max);
    }

    return max-min;


}


uint64_t day14part2(std::basic_istream<char>& input){
    auto [polymerStr, insertions] = getPolymerInsertions(input);

    std::unordered_map<std::string, uint64_t> polymer;
    for (int i = 0; i < polymerStr.size() - 1; ++i) {
        std::string pairPol(polymerStr.c_str() + i, 2);
        ++polymer[pairPol];
    }


    for( int it = 0; it < 40; ++it) {
        decltype(polymer) newPolymer;
        for (const auto& [polyPair, count]: polymer) {
            std::string polyPair1 = polyPair; polyPair1[1] = insertions[polyPair];
            std::string polyPair2 = polyPair; polyPair2[0] = insertions[polyPair];

            newPolymer[polyPair1] += count;
            newPolymer[polyPair2] += count;
        }
        polymer = std::move(newPolymer);
    }

    std::unordered_map<char, uint64_t> polyMap;
    for( const auto& [polyPair, count]: polymer){
        polyMap[polyPair[0]] += count;
    }
    ++polyMap[polymerStr.back()];

    uint64_t min = std::numeric_limits<uint64_t>::max();
    uint64_t max = 0;
    for( const auto& [_, count]: polyMap){
        min = std::min(count, min);
        max = std::max(count, max);
    }

    return max-min;

}




constexpr std::string_view smallTest1 = R"(NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C)";

TEST_CASE( "Day 14 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day14part1(test1) == 1588);
    }


    SECTION("Day 14") {
        std::ifstream realData("../input_14.txt");
        fmt::print("Day 14, Part 1: {}\n", day14part1(realData));
    }

}

TEST_CASE( "Day 14 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day14part2(test1) == 2188189693529);
    }

    SECTION("Day 14") {
        std::ifstream realData("../input_14.txt");
        fmt::print("Day 14, Part 2: {}\n", day14part2(realData));
    }

}
