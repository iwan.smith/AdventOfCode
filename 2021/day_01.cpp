#include <sstream>
#include <fstream>

#include "catch2/catch.hpp"
#include "fmt/format.h"

int day01part1(std::basic_istream<char>& depths){

    int nIncreasing = 0;

    int depth{0}; depths >> depth;
    for (int oldDepth = depth; depths >> depth; ){
        if (depth > oldDepth)
            ++nIncreasing;
        oldDepth = depth;
    }
    return nIncreasing;
}

int day01part2(std::basic_istream<char>& depths){

    int nIncreasing = 0;

    std::vector<int> depth(3,0);

    depths >> depth[0];
    depths >> depth[1];
    depths >> depth[2];

    int oldDepth = std::numeric_limits<int>::max();
    do{

        int avgDepth = std::accumulate(depth.begin(), depth.end(), 0);

        if (avgDepth > oldDepth)
            ++nIncreasing;
        oldDepth = avgDepth;

        depth.erase(depth.begin());
        depth.push_back(0);
    }while (depths >> depth[2]);

    return nIncreasing;
}


TEST_CASE( "Day 01 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1("199\n200\n208\n210\n200\n207\n240\n269\n260\n263");
        CHECK(day01part1(test1) == 7);
    }

    SECTION("Day 01") {
        std::ifstream realData("../input_01.txt");
        fmt::print("Day 01, Part 1: {}\n", day01part1(realData));
    }

}

TEST_CASE( "Day 01 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1("199\n200\n208\n210\n200\n207\n240\n269\n260\n263");
        CHECK(day01part2(test1) == 5);
    }

    SECTION("Day 01") {
        std::ifstream realData("../input_01.txt");
        fmt::print("Day 01, Part 2: {}\n", day01part2(realData));
    }

}
