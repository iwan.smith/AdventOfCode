#include <sstream>
#include <fstream>
#include <optional>
#include <unordered_set>

#include "catch2/catch.hpp"
#include "fmt/format.h"

using RotationMatrix = std::array<std::array<int, 3>, 3>;
constexpr std::array<RotationMatrix, 24> getOrientations(){

    constexpr std::array<int, 4> c{1, 0, -1, 0};
    constexpr std::array<int, 4> s{0, 1, 0, -1};

    std::array<RotationMatrix, 4*4*4> rotations{};
    for( int a = 0; a < 4; ++a){
        for( int b = 0; b < 4; ++b) {
            for (int g = 0; g < 4; ++g) {
                rotations[a*4*4 + b*4 + g] = {{
                        { c[a]*c[b], c[a]*s[b]*s[g]-s[a]*c[g], c[a]*s[b]*c[g]+s[a]*s[g]},
                        { s[a]*c[b], s[a]*s[b]*s[g]+c[a]*c[g], s[a]*s[b]*c[g]-c[a]*s[g]},
                        { -s[b]    , c[b]*s[g]               , c[b]*c[g]               }
                }};
            }
        }
    }
    std::array<RotationMatrix, 24> orientations{};

    std::ranges::sort(rotations);
    std::ranges::unique_copy(rotations, orientations.begin());

    return orientations;
}

struct XYZ{
    int x, y, z;
    auto operator <=>(const XYZ&) const = default;
};


namespace std {
    template <> struct hash<XYZ>
    {
        uint64_t operator()(const XYZ& xyz) const
        {
            uint64_t x = static_cast<uint16_t>(xyz.x);
            uint64_t y = static_cast<uint16_t>(xyz.y);
            uint64_t z = static_cast<uint16_t>(xyz.z);
            return x + (y<<16) + (z<<32);
        }
    };
}

struct Scanner{
    std::vector<XYZ> beacons;
    std::optional<XYZ> xyz;
    std::optional<RotationMatrix> orientation;
};


XYZ rotate(const RotationMatrix& r, XYZ xyz){
    return {
            r[0][0] * xyz.x + r[0][1] * xyz.y + r[0][2] * xyz.z,
            r[1][0] * xyz.x + r[1][1] * xyz.y + r[1][2] * xyz.z,
            r[2][0] * xyz.x + r[2][1] * xyz.y + r[2][2] * xyz.z,
    };
}


void findOffsetAndOrientation(const Scanner& s1, Scanner& s2){
    static constexpr auto orientations = getOrientations();
    for( const auto& r: orientations){
        std::unordered_map<XYZ, int> offsets;
        for( const auto& b1: s1.beacons){
            for(const auto& b2: s2.beacons){
                auto b1r = rotate(s1.orientation.value(), b1);
                auto b2r = rotate(r, b2);
                XYZ offset{-b2r.x+b1r.x, -b2r.y+b1r.y, -b2r.z+b1r.z};
                if ( ++offsets[offset] == 12){
                    s2.xyz = XYZ{offset.x+s1.xyz.value().x, offset.y+s1.xyz.value().y, offset.z+s1.xyz.value().z};
                    s2.orientation = r;
                    return;
                }
            }
        }

    }
}

std::pair<int, int> day19(std::basic_istream<char>& input){

    std::vector<Scanner> scanners;
    Scanner scanner;
    for( std::string line; std::getline(input,line);){
        int scannerId;
        if( sscanf( line.c_str(), "--- scanner %d ---", &scannerId) == 1 && !scanner.beacons.empty()){
            scanners.emplace_back(std::move(scanner));
            scanner.beacons.clear();
        }
        int x{0}, y{0}, z{0};
        if( sscanf( line.c_str(), "%d,%d,%d", &x, &y, &z) == 3){
            scanner.beacons.emplace_back(XYZ{x,y,z});
        }
    }
    scanners.emplace_back(std::move(scanner));

    static constexpr auto orientations = getOrientations();

    scanners[0].xyz         = XYZ{0,0,0};
    scanners[0].orientation = orientations[0];

    auto isFinished = [&](){
        for( const auto& s:scanners)
            if (!s.xyz.has_value()) return false;
        return true;
    };

    while (!isFinished()) {
        for (const auto &s1: scanners) {
            for (auto &s2: scanners) {
                if (s2.xyz.has_value()) continue;
                if (!s1.xyz.has_value()) continue;
                findOffsetAndOrientation(s1, s2);
            }
        }
    }

    std::unordered_set<XYZ> locations;
    for( const auto& [beacons, off, r]: scanners){
        for( const auto& b: beacons) {
            auto rb = rotate(r.value(), b);
            XYZ xyz{rb.x + off.value().x, rb.y + off.value().y, rb.z + off.value().z};
            locations.insert(xyz);
        }
    }

    int maxDist = 0;
    for( const auto& s1: scanners) {
        for (const auto &s2: scanners) {
            const auto& [x1, y1, z1] = s1.xyz.value();
            const auto& [x2, y2, z2] = s2.xyz.value();

            maxDist = std::max(maxDist, std::abs(x1 - x2) + std::abs(y1 - y2) + std::abs(z1 - z2));
        }
    }

    return {locations.size(), maxDist};
}


constexpr std::string_view smallTest1 = R"(--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14)";

TEST_CASE( "Day 19", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1) };
        const auto& [part1, part2] = day19(test1);
        CHECK( part1 == 79);
        CHECK( part2 == 3621);
    }

    SECTION("Day 19") {
        std::ifstream realData("../input_19.txt");
        const auto& [part1, part2] = day19(realData);

        fmt::print("Day 19, Part 1: {}\n", part1);
        fmt::print("Day 19, Part 2: {}\n", part2);
    }

}
