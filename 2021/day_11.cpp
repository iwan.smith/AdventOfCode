#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>

#include "catch2/catch.hpp"
#include "fmt/format.h"
#include <fmt/ranges.h>



void flash(size_t x, size_t y, std::vector<std::string>& octopi){
    octopi[x][y] = '0';
    for( const auto& dx: {-1, 0, 1}) {
        for (const auto &dy: {-1, 0, 1}) {
            if (!(dx == 0 && dy == 0)) {
                if (x + dx >= 10)
                    continue;
                if (y + dy >= 10)
                    continue;

                if ( octopi[x + dx][y + dy] > '0' && octopi[x + dx][y + dy] < '0'+10)
                    ++octopi[x + dx][y + dy];

                if ( octopi[x + dx][y + dy] == '0'+10 )
                    flash(x+dx, y+dy, octopi);


            }
        }
    }
}

int day11part1(std::basic_istream<char>& input){
    std::vector<std::string> octopi;

    for(std::string line; std::getline(input, line); ){
        octopi.push_back(line);
    }

    int score = 0;
    for ( size_t it = 0; it < 100; ++it ){

        for( size_t x = 0; x < 10; ++x)
            for( size_t y = 0; y < 10; ++y)
                ++octopi[x][y];

        for (size_t x = 0; x < 10; ++x)
            for (size_t y = 0; y < 10; ++y)
                if (octopi[x][y] == '0'+10)
                    flash(x, y, octopi);

        for( size_t x = 0; x < 10; ++x)
            for( size_t y = 0; y < 10; ++y)
                if(octopi[x][y] == '0') {
                    ++score;
                }

    }


    return score;
}


int day11part2(std::basic_istream<char>& input){

    std::vector<std::string> octopi;
    const std::vector<std::string> all0(10, std::string(10, '0'));

    for(std::string line; std::getline(input, line); ){
        octopi.push_back(line);
    }

    int step = 0;
    for ( ; octopi != all0 ; ++step ){

        for( size_t x = 0; x < 10; ++x)
            for( size_t y = 0; y < 10; ++y)
                ++octopi[x][y];

        for (size_t x = 0; x < 10; ++x)
            for (size_t y = 0; y < 10; ++y)
                if (octopi[x][y] == '0'+10)
                    flash(x, y, octopi);

    }


    return step;
}




constexpr std::string_view smallTest = R"(5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526)";

TEST_CASE( "Day 11 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day11part1(test1) == 1656);
    }

    SECTION("Day 11") {
        std::ifstream realData("../input_11.txt");
        fmt::print("Day 11, Part 1: {}\n", day11part1(realData));
    }

}

TEST_CASE( "Day 11 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day11part2(test1) == 195);
    }

    SECTION("Day 11") {
        std::ifstream realData("../input_11.txt");
        fmt::print("Day 11, Part 2: {}\n", day11part2(realData));
    }

}
