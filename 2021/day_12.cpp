#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>

#include "catch2/catch.hpp"
#include "fmt/format.h"
#include <fmt/ranges.h>

using Nodes = std::unordered_map<std::string, std::vector<std::string>>;

int countAllRoutes(const Nodes& nodes, std::vector<std::string> route = {}, const std::string& currNode = "start"){
    // if node is already used and is lower case. don't continue
    if ( std::ranges::count(route, currNode) == 1){
        if (std::islower(currNode[0])) {
            return 0;
        }
    }

    // if we have reached the end, return 1
    if (currNode == "end"){
        return 1;
    }

    int nRoute = 0;

    route.push_back(currNode);
    for( const auto& node: nodes.at(currNode)){
            nRoute += countAllRoutes(nodes, route, node);
    }
    return nRoute;
}

int day12part1(std::basic_istream<char>& input){

    Nodes nodes;
    for(std::string line; std::getline(input, line);){
        char c1[64], c2[64];
        sscanf(line.data(), "%[^-]-%[^-]", c1, c2);
        nodes[&c1[0]].push_back(&c2[0]);
        nodes[&c2[0]].push_back(&c1[0]);
    }

    
    return countAllRoutes(nodes);
}

int countAllRoutes2(const Nodes& nodes, std::vector<std::string> route = {}, const std::string& currNode = "start", bool smallVisitedTwice = false){

    // if we have reached the end, return 1
    if (currNode == "end"){
        return 1;
    }
    // if we traverse back to start, return 0
    if (currNode == "start" && route.size()>0){
        return 0;
    }

    if (std::islower(currNode[0])) {

        int nVisited = std::ranges::count(route, currNode);

        if (nVisited == 2){
            return 0;
        }

        if (  nVisited == 1 ){
            if( !smallVisitedTwice){
            smallVisitedTwice = true;
            }else {
                return 0;
            }
        }
    }


    int nRoute = 0;

    route.push_back(currNode);
    for( const auto& node: nodes.at(currNode)){
        nRoute += countAllRoutes2(nodes, route, node, smallVisitedTwice);
    }
    return nRoute;
}

int day12part2(std::basic_istream<char>& input){

    Nodes nodes;
    for(std::string line; std::getline(input, line);){
        char c1[64], c2[64];
        sscanf(line.data(), "%[^-]-%[^-]", c1, c2);
        nodes[&c1[0]].push_back(&c2[0]);
        nodes[&c2[0]].push_back(&c1[0]);
    }


    return countAllRoutes2(nodes);
}




constexpr std::string_view smallTest1 = R"(dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc)";

constexpr std::string_view smallTest2 = R"(fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW)";

TEST_CASE( "Day 12 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day12part1(test1) == 19);
    }

    SECTION("Unit Test 2") {
        std::stringstream test1{std::string(smallTest2)};
        CHECK(day12part1(test1) == 226);
    }

    SECTION("Day 12") {
        std::ifstream realData("../input_12.txt");
        fmt::print("Day 12, Part 1: {}\n", day12part1(realData));
    }

}

TEST_CASE( "Day 12 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day12part2(test1) == 103);
    }

    SECTION("Unit Test 2") {
        std::stringstream test1{std::string(smallTest2)};
        CHECK(day12part2(test1) == 3509);
    }

    SECTION("Day 12") {
        std::ifstream realData("../input_12.txt");
        fmt::print("Day 12, Part 2: {}\n", day12part2(realData));
    }

}
