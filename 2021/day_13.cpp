#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <unordered_set>

#include "catch2/catch.hpp"
#include "fmt/format.h"


auto getPaper_Folds(std::basic_istream<char>& input){

    std::unordered_set<uint64_t> paper;
    for(std::string line; std::getline(input, line);) {
        if (line.empty())
            break;

        std::stringstream ss(line);
        uint64_t x{0}, y{0};
        char ignoreComma;
        ss >> x;
        ss >> ignoreComma;
        ss >> y;

        paper.insert((x<<32)+y);
    }

    std::vector<std::pair<char, int>> folds;
    for(std::string line; std::getline(input, line);) {
        folds.emplace_back();
        sscanf(line.data(), "fold along %1c=%d", &folds.back().first, &folds.back().second);
    }

    return std::pair(paper, folds);

}

std::unordered_set<uint64_t> foldPaper(const std::unordered_set<uint64_t>& paper, std::pair<char, int> fold){

    std::unordered_set<uint64_t> ret;

    const auto& [foldXY, foldCoord] = fold;
    for( const auto& dot: paper){
        uint64_t x = dot>>32;
        uint64_t y = static_cast<uint32_t>(dot);

        if ( foldXY == 'x' && x > foldCoord){

            ret.insert(((2*foldCoord-x)<<32)+y);
        } else if ( foldXY == 'y' && y > foldCoord) {
            ret.insert((x << 32) + (2*foldCoord-y));
        } else {
            ret.insert(dot);
        }
    }
    return ret;

}

size_t day13part1(std::basic_istream<char>& input){

    auto [paper, folds] = getPaper_Folds(input);

    paper = foldPaper(paper, folds[0]);

    return paper.size();
}


void day13part2(std::basic_istream<char>& input){
    auto [paper, folds] = getPaper_Folds(input);

    for (const auto& fold:folds)
        paper = foldPaper(paper, fold);

    for( uint64_t y = 0; y < 7; ++y) {
        for( uint64_t x = 0; x < 50; ++x){
            fmt::print("{}", paper.contains((x << 32) + y) ? "██" : "  ");
        }
        fmt::print("\n");
    }
}




constexpr std::string_view smallTest1 = R"(6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5)";

TEST_CASE( "Day 13 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day13part1(test1) == 17);
    }


    SECTION("Day 13") {
        std::ifstream realData("../input_13.txt");
        fmt::print("Day 13, Part 1: {}\n", day13part1(realData));
    }

}

TEST_CASE( "Day 13 Part 2", "AOC" ) {

    SECTION("Day 13") {
        std::ifstream realData("../input_13.txt");
        fmt::print("Day 13, Part 2:\n");
        day13part2(realData);
    }

}
