#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>

#include "catch2/catch.hpp"

#include "fmt/format.h"

namespace {
    std::vector<std::string> step( std::vector<std::string> sea ){
        auto ret = sea;
        const size_t nx = sea.front().size();
        const size_t ny = sea.size();

        for( size_t i = 0; i < ny; ++i){
            for( size_t j = 0; j < nx; ++j){
                if ( sea[i][j] == '>' && sea[i][(j+1)%nx] == '.' ) {
                    ret[i][(j + 1) % nx] = '>';
                    ret[i][j] = '.';
                }
            }
        }
        sea = ret;
        for( size_t i = 0; i < ny; ++i){
            for( size_t j = 0; j < nx; ++j){
                if ( sea[i][j] == 'v' && sea[(i+1)%ny][j] == '.' ) {
                    ret[(i+1)%ny][j] = 'v';
                    ret[i][j] = '.';
                }
            }
        }
        return ret;
    }

}


TEST_CASE("Day 25 Test One Step"){

    SECTION("STEP 1"){
        std::vector<std::string> before{
            "...>...",
            ".......",
            "......>",
            "v.....>",
            "......>",
            ".......",
            "..vvv..",
        };
        std::vector<std::string> after{
            "..vv>..",
            ".......",
            ">......",
            "v.....>",
            ">......",
            ".......",
            "....v..",
        };

        CHECK(step(before) == after);

    }
}

int64_t day25part1(std::basic_istream<char>& input) {
    std::vector<std::string> seaFloor;

    for (std::string line; std::getline(input, line); ) {
        seaFloor.emplace_back(std::move(line));
    }

    int nStep = 0;
    std::vector<std::string> nextSeaFloor = seaFloor;
    do {
        seaFloor = std::move(nextSeaFloor);
        ++nStep;
        nextSeaFloor = step(seaFloor);
    } while (nextSeaFloor != seaFloor);

    return nStep;
}

int64_t day25part2(std::basic_istream<char>& input){

}

constexpr std::string_view unitTest1 = R"(v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>)";

TEST_CASE( "Day 25 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(unitTest1) };
        CHECK(day25part1(test1) == 58);
    }


    SECTION("Day 25") {
        std::ifstream realData("../input_25.txt");
        fmt::print("Day 25, Part 1: {}\n", day25part1(realData));
    }
}

