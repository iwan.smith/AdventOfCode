#include <sstream>
#include <fstream>
#include <set>
#include <optional>

#include "catch2/catch.hpp"
#include "fmt/format.h"

int day07part1(std::basic_istream<char>& inputCrab){

    std::unordered_map<int,int> crabPositions;

    char ignoreComma;
    for( uint32_t crab; inputCrab >> crab; inputCrab >> ignoreComma){
        ++crabPositions[crab];
    }

    int minfuel = std::numeric_limits<int>::max();
    for( int position = 0; ; ++position){
        int fuel = 0;
        for( const auto& [crabPos, nCrab]: crabPositions){
            fuel += std::abs(crabPos-position)*nCrab;
        }

        if ( fuel > minfuel)
            break;
        else
            minfuel = fuel;
    }

    return minfuel;

}

uint64_t day07part2(std::basic_istream<char>& inputCrab){


    std::unordered_map<int,int> crabPositions;

    int sumPositions = 0;
    int count = 0;

    char ignoreComma;
    for( uint32_t crab; inputCrab >> crab; inputCrab >> ignoreComma){
        ++crabPositions[crab];
        sumPositions += crab;
        ++count;
    }

    int minfuel = std::numeric_limits<int>::max();
    for( const auto dx: {-1, 1}) {
        for (int position = sumPositions/count; ; position += dx) {

            int fuel = 0;
            for (const auto&[crabPos, nCrab]: crabPositions) {
                int dist = std::abs(crabPos-position);
                fuel += dist*(dist+1)/2 * nCrab;
            }

            if (fuel > minfuel)
                break;
            else
                minfuel = fuel;
        }
    }

    return minfuel;

}




constexpr std::string_view smallTest = R"(16,1,2,0,4,2,7,1,2,14)";

TEST_CASE( "Day 07 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day07part1(test1) == 37);
    }

    SECTION("Day 07") {
        std::ifstream realData("../input_07.txt");
        fmt::print("Day 07, Part 1: {}\n", day07part1(realData));
    }

}

TEST_CASE( "Day 07 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day07part2(test1) == 168);
    }

    SECTION("Day 07") {
        std::ifstream realData("../input_07.txt");
        fmt::print("Day 07, Part 2: {}\n", day07part2(realData));
    }

}
