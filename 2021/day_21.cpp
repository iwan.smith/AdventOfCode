#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <iostream>
#include <complex>

#include "catch2/catch.hpp"
#include "fmt/format.h"

class Dice{
public:
    int get(){
        if ( ++dice > 100) dice = 1;
        ++rolls;
        return dice;
    }
    int rolls = 0;
private:
    int dice = 0;
};

auto getPositions(std::basic_istream<char>& input){
    int pos_1, pos_2;

    std::string line;
    std::getline(input, line);
    sscanf(line.c_str(), "Player 1 starting position: %d", &pos_1);
    std::getline(input, line);
    sscanf(line.c_str(), "Player 2 starting position: %d", &pos_2);

    return std::pair{pos_1, pos_2};
}

uint64_t day21part1(std::basic_istream<char>& input){
    auto [pos_1, pos_2] = getPositions(input);
    int score_1{0}, score_2{0};

    Dice d;
    for (bool player1 =true; score_1 < 1000 && score_2 < 1000; player1=!player1){
        int& pos   = player1 ? pos_1   : pos_2;
        int& score = player1 ? score_1 : score_2;

        int roll = d.get()+d.get()+d.get();
        pos = (pos+roll)%10;
        if (pos == 0) pos = 10;

        score += pos;
    }

    return std::min(score_2, score_1) * d.rolls;


}

consteval auto getDiceCombinations(){
    std::array<unsigned, 10> hist{};
    for( int i = 1; i <= 3; ++i)
        for( int j = 1; j <= 3; ++j)
            for( int k = 1; k <= 3; ++k)
                ++hist[i+j+k];

    std::array<std::pair<uint32_t, uint32_t>, 7> combs;
    for( int i = 3; i <= 9; ++i ){
        combs[i-3] = {i, hist[i]};
    }
    return combs;
}

uint64_t day21part2(std::basic_istream<char>& input){
    auto [pos_1, pos_2] = getPositions(input);
    uint64_t wins_1{0}, wins_2{0};

    std::vector<uint64_t> scoresAndPositions1(21*10, 0 );
    std::vector<uint64_t> scoresAndPositions2(21*10, 0 );
    scoresAndPositions1[pos_1-1] = 1;
    scoresAndPositions2[pos_2-1] = 1;


    for (bool player1 =true; ; player1=!player1){

        std::vector<uint64_t>&  scoreAndPos = player1 ? scoresAndPositions1 : scoresAndPositions2;
        std::vector<uint64_t>&  other       = player1 ? scoresAndPositions2 : scoresAndPositions1;
        uint64_t&               wins        = player1 ? wins_1              : wins_2;

        std::vector<uint64_t> nextScoreAndPos(21*10, 0);
        for( int pos = 0; pos < 10; ++pos){
            for( int score = 0; score < 21; ++score ){
                for( const auto&[ roll, freq] : getDiceCombinations()){
                    uint64_t newPos = (pos+roll)%10;
                    uint64_t newScore = score + newPos + 1;

                    if ( newScore >= 21 )
                        wins += scoreAndPos[score*10+pos] * freq * std::accumulate(other.begin(), other.end(), uint64_t{0});
                    else {
                        assert ( newScore >= 0 && newScore < 21);
                        assert ( newPos >= 0 && newPos < 10);
                        nextScoreAndPos[newScore*10 + newPos] += scoreAndPos[score * 10 + pos] * freq;
                    }
                }
            }
        }

        scoreAndPos = std::move(nextScoreAndPos);
        if ( std::ranges::all_of(scoreAndPos, [](auto s){return s==0;}) )
            break;
    }

    return std::max(wins_1, wins_2);


}

constexpr std::string_view smallTestInput = R"(Player 1 starting position: 4
Player 2 starting position: 8)";

TEST_CASE( "Day 21 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTestInput)};
        CHECK(day21part1(test1) == 739785);
    }


    SECTION("Day 21") {
        std::ifstream realData("../input_21.txt");
        fmt::print("Day 21, Part 1: {}\n", day21part1(realData));
    }

}

TEST_CASE( "Day 21 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTestInput)};
        CHECK(day21part2(test1) == 444356092776315);
    }


    SECTION("Day 21") {
        std::ifstream realData("../input_21.txt");
        fmt::print("Day 21, Part 2: {}\n", day21part2(realData));
    }

}
