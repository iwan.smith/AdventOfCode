#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <ranges>
#include <unordered_set>
#include <queue>

#include "catch2/catch.hpp"
#include "fmt/format.h"


class BitReader{
public:
    BitReader(std::basic_istream<char>& input)
    :m_input(input){}

    uint64_t readNBits(unsigned n){
        m_bits += n;

        uint64_t ret = 0;
        while ( n --> 0 ){
            if (mask-- == 0 ){
                char hex;

                if ( !(m_input>>hex) )
                    throw(std::runtime_error("Out of Input"));

                mask = 3;
                if ( hex >= '0' && hex <= '9')
                    curr = hex-'0';
                else
                    curr = hex - 'A' + 10;
            }
            ret = (ret<<1) + ((curr&0b1000) == 0 ? 0 : 1);
            curr = curr<<1;
        }
        return ret;
    }


    std::pair<uint64_t, uint64_t> parsePacket(int depth = 0 ) {

        ++m_packets;

        uint8_t version = readNBits(3);
        uint8_t type = readNBits(3);


        if (type == 4) {
            uint64_t value = 0;
            uint64_t bits5;
            do {
                bits5 = readNBits(5);
                value = (value << 4) + (bits5 & 0b1111);
            } while (bits5 & 0b10000);

            return {version, value};
        }


        std::pair<uint64_t, uint64_t> ret = {version, 0};
        std::vector<uint64_t> vals;

        uint8_t lengthID = readNBits(1);
        if (lengthID == 0) {
            uint32_t end = readNBits(15);
            end += m_bits;
            while (m_bits < end) {
                auto sub = parsePacket(depth+1);
                ret.first += sub.first;
                vals.push_back(sub.second);
            }
        } else{
            uint32_t nPack = readNBits(11);
            for (int p = 0; p < nPack; ++p) {
                auto sub = parsePacket(depth+1);
                ret.first += sub.first;
                vals.push_back(sub.second);
            }
        }
        switch(type){
            case 0: ret.second = std::accumulate(vals.begin(), vals.end(), uint64_t(0), std::plus<>()); break;
            case 1: ret.second = std::accumulate(vals.begin(), vals.end(), uint64_t(1), std::multiplies<>()); break;
            case 2: ret.second = std::ranges::min(vals); break;
            case 3: ret.second = std::ranges::max(vals); break;
            case 5: assert(vals.size() == 2); ret.second = (vals[0] >  vals[1]) ? 1 : 0; break;
            case 6: assert(vals.size() == 2); ret.second = (vals[0] <  vals[1]) ? 1 : 0; break;
            case 7: assert(vals.size() == 2); ret.second = (vals[0] == vals[1]) ? 1 : 0; break;
            default: assert(false);
        }

        return ret;

    }



    std::basic_istream<char>& m_input;

    uint8_t curr = 0;
    uint8_t mask = 0;

    uint32_t m_bits = 0;
    uint32_t m_packets = 0;

};




uint64_t day16part1(std::basic_istream<char>& input){
    BitReader reader(input);

    return reader.parsePacket().first;
}


uint64_t day16part2(std::basic_istream<char>& input){
    BitReader reader(input);

    return reader.parsePacket().second;

}



constexpr std::string_view smallTest1 = R"(8A004A801A8002F478)";
constexpr std::string_view smallTest2 = R"(620080001611562C8802118E34)";
constexpr std::string_view smallTest3 = R"(C0015000016115A2E0802F182340)";
constexpr std::string_view smallTest4 = R"(A0016C880162017C3686B18A3D4780)";

TEST_CASE( "Day 16 Unit Tests", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string("D2FE28")};
        CHECK(day16part1(test1) == 6);
    }

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string("38006F45291200")};
        CHECK(day16part1(test1) == 9);
    }

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string("EE00D40C823060")};
        CHECK(day16part1(test1) == 14);
    }

}

TEST_CASE( "Day 16 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest1)};
        CHECK(day16part1(test1) == 16);
    }

    SECTION("Unit Test 2") {
        std::stringstream test2{std::string(smallTest2)};
        CHECK(day16part1(test2) == 12);
    }

    SECTION("Unit Test 3") {
        std::stringstream test3{std::string(smallTest3)};
        CHECK(day16part1(test3) == 23);
    }

    SECTION("Unit Test 4") {
        std::stringstream test4{std::string(smallTest4)};
        CHECK(day16part1(test4) == 31);
    }

    SECTION("Day 16") {
        std::ifstream realData("../input_16.txt");
        fmt::print("Day 16, Part 1: {}\n", day16part1(realData));
    }

}

TEST_CASE( "Day 16 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{"C200B40A82"};
        CHECK(day16part2(test1) == 3);
    }

    SECTION("Unit Test 2") {
        std::stringstream test2{"04005AC33890"};
        CHECK(day16part2(test2) == 54);
    }

    SECTION("Unit Test 3") {
        std::stringstream test3{"880086C3E88112"};
        CHECK(day16part2(test3) == 7);
    }

    SECTION("Unit Test 4") {
        std::stringstream test4{"CE00C43D881120"};
        CHECK(day16part2(test4) == 9);
    }

    SECTION("Unit Test 5") {
        std::stringstream test5{"D8005AC2A8F0"};
        CHECK(day16part2(test5) == 1);
    }

    SECTION("Unit Test 6") {
        std::stringstream test6{"F600BC2D8F"};
        CHECK(day16part2(test6) == 0);
    }

    SECTION("Unit Test 7") {
        std::stringstream test7{"9C005AC2F8F0"};
        CHECK(day16part2(test7) == 0);
    }

    SECTION("Unit Test 8") {
        std::stringstream test7{"9C0141080250320F1802104A08"};
        CHECK(day16part2(test7) == 1);
    }

    SECTION("Day 16") {
        std::ifstream realData("../input_16.txt");
        fmt::print("Day 16, Part 2: {}\n", day16part2(realData));
    }
//
}
