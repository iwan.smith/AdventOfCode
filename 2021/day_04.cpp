#include <sstream>
#include <fstream>
#include <set>
#include <optional>

#include "catch2/catch.hpp"
#include "fmt/format.h"


std::tuple<std::vector<int>, std::vector<std::array<int, 25>>> processInput(std::basic_istream<char>& bingoData){

    std::vector<int> retNumbers;
    std::vector<std::array<int, 25>> retBoards;

    std::string numbers;
    std::getline(bingoData, numbers);
    std::stringstream numbersStr(numbers);

    {
        int numIn{0};
        char ignore{0};
        while (numbersStr >> numIn) {
            numbersStr >> ignore;
            retNumbers.push_back(numIn);
        }
    }

    auto getBoard = [&]()->std::optional<std::array<int, 25>>{
        std::array<int, 25> game{};
        for (int i = 0; i < 25; ++i) {
            if (!(bingoData >> game[i]))
                return std::optional<std::array<int, 25>>{};
        }
        return game;
    };

    while (auto game = getBoard()){
        retBoards.emplace_back(game.value());
    }

    return {retNumbers, retBoards};

}

int scoreBoard(const std::set<int>& drawnNumbers, const std::array<int, 25>& board){

    // test rows
    bool win = false;
    int score = 0;
    for( int i = 0; i < 25; i+=5) {
        unsigned n = 0;
        for (int j = 0; j < 5; ++j) {
            if ( drawnNumbers.contains(board[i+j]) ) ++n;
            else score += board[i+j];
        }
        if (n == 5) win = true;
    }

    for( int i = 0; i < 5; ++i) {
        unsigned n = 0;
        for (int j = 0; j < 25; j+=5) {
            if ( drawnNumbers.contains(board[i+j]) ) ++n;
        }
        if (n == 5) win = true;
    }

    if ( win )
        return score;
    return 0;

}


int day04part1(std::basic_istream<char>& bingoData){
    const auto& [numbers, games] = processInput(bingoData);

    std::set<int> drawnNumbers;
    for ( const auto& num: numbers){
        drawnNumbers.insert(num);
        for(const auto& game: games){
            int score = scoreBoard(drawnNumbers, game);
            if ( score )
                return num * score;
        }
    }

    return 0;
}

int day04part2(std::basic_istream<char>& bingoData){
    const auto& [numbers, games] = processInput(bingoData);

    std::vector<bool> wonGames(games.size(), false);
    int nWon = 0;

    std::set<int> drawnNumbers;

    for ( const auto& num: numbers){
        drawnNumbers.insert(num);
        for(int i = 0; i < games.size(); ++i){
            if ( wonGames[i])
                continue;

            int score = scoreBoard(drawnNumbers, games[i]);
            if ( score ) {
                wonGames[i] = true;
                ++nWon;
                if (nWon == games.size())
                    return num * score;
            }
        }
    }

    return 0;
}



constexpr std::string_view smallTest = R"(7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
8  2 23  4 24
21  9 14 16  7
6 10  3 18  5
1 12 20 15 19

3 15  0  2 22
9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
2  0 12  3  7
)";

TEST_CASE( "Day 04 Part 1", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day04part1(test1) == 4512);
    }

    SECTION("Day 04") {
        std::ifstream realData("../input_04.txt");
        fmt::print("Day 04, Part 1: {}\n", day04part1(realData));
    }

}

TEST_CASE( "Day 04 Part 2", "AOC" ) {

    SECTION("Unit Test 1") {
        std::stringstream test1{std::string(smallTest)};
        CHECK(day04part2(test1) == 1924);
    }

    SECTION("Day 04") {
        std::ifstream realData("../input_04.txt");
        fmt::print("Day 04, Part 2: {}\n", day04part2(realData));
    }

}
