#include <ranges>
#include <iostream>
#include <algorithm>
#include <unordered_map>

int main(){


  std::vector<int> adapters{0};
  adapters.insert(adapters.end(), std::istream_iterator<int>(std::cin), std::istream_iterator<int>());
  std::ranges::sort(adapters);
  adapters.push_back(adapters.back() + 3);

  std::unordered_map<unsigned, unsigned> jumps;
  unsigned last = 0;
  for( const auto& i : adapters)
  {
    ++jumps[i-last];
    last = i;
  }

  std::cout << "Part 1: " << jumps[1] * (jumps[3]) << "\n";


  std::unordered_map<unsigned, unsigned long> combMap{{adapters.back(), 1}};
  for(unsigned i = adapters.size() - 2; i < adapters.size() ; --i)
  {
    unsigned long combs = 0;
    for (unsigned j=i+1; j<adapters.size() && adapters[j]<=adapters[i]+3 ; ++j)
    {
      combs += combMap[adapters[j]];
    }
    combMap[adapters[i]] = combs;
  }

  std::cout << "Part 2: " << combMap[0] << "\n";

}
