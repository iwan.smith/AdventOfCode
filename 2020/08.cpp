#include <unordered_set>
#include <iostream>
#include <vector>

struct instr
{
  enum op{nop,acc,jmp};

  op o;
  int arg;
};

auto runProgram(const auto& program)
{


  long int accumulator = 0;
  unsigned instrCount  = 0;
  std::vector<int> instrCache(program.size(), 0);
  
  while ( instrCount < program.size() && instrCache[instrCount]++ == 0)
  {
    switch(program[instrCount].o){
      case instr::op::nop: 
        ++instrCount;
        break;
      case instr::op::acc:
        accumulator += program[instrCount].arg;
        ++instrCount;
        break;
      case instr::op::jmp:
        instrCount += program[instrCount].arg;
        break;
    }
  }

  return std::make_pair(instrCount==program.size(), accumulator);
}


int main(){

  std::vector<instr> program = []()
  {
    std::vector<instr> ret;

    char o[4];
    int arg;
    while (scanf("%s %d", o, &arg) == 2)
    {
      using namespace std::literals::string_literals;
      if      ( o == "nop"s ) ret.emplace_back(instr{instr::op::nop, arg});  
      else if ( o == "jmp"s ) ret.emplace_back(instr{instr::op::jmp, arg});  
      else if ( o == "acc"s ) ret.emplace_back(instr{instr::op::acc, arg});  
    } 
  
    return ret;
  }();

  auto [ exit, acc] = runProgram(program);
  std::cout << acc << "\n";
  
  for ( unsigned i = 0; i < program.size(); ++i )
  {
    if ( program[i].o != instr::op::nop )
    {
      program[i].o ^= (instr::op::nop^instr::op::jmp);
      auto [ exit, acc] = runProgram(program);
      if (exit)
      {
        std::cout << acc << "\n";
        break;
      }
      program[i].o ^= (instr::op::nop^instr::op::jmp);
    }
  }
  
}
