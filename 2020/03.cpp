#include <vector>
#include <string>
#include <iostream>

int main()
{

  const std::vector<std::string> forest = [](){
    std::vector<std::string> ret; 
    std::string line;
    while ( getline(std::cin, line) )
      ret.emplace_back(std::move(line));
    return ret;
  }();

  std::vector<std::pair<int,int>> directions = { {1,1}, {3,1}, {5,1}, {7,1}, {1,2}};

  unsigned long totMul = 1;
  for( const auto& [right, down] : directions )
  {
    int nTrees = 0;
    for( int i(0), j(0); i < forest.size(); i+=down )
    {
      nTrees += (forest[i][j] == '#');
      j = (j+right)%forest[0].size();
    }
    
    if ( right == 3 )
      std::cout << "Part 1: " << nTrees << std::endl;
    totMul *= nTrees;
  }

  std::cout << "Part 2: " << totMul << std::endl;

}
