#include <unordered_set>
#include <unordered_map>
#include <iostream>



int main()
{
  
  unsigned sumQs = 0;
  unsigned sumQs2 = 0;

  std::unordered_map<char, unsigned> Qs;
  unsigned nPeople = 0;
  for(std::string line; std::getline(std::cin, line); )
  {
    if ( line.empty() )
    {
      sumQs += Qs.size();
      for( const auto [q,n] : Qs )
        sumQs2 += n == nPeople;
      Qs.clear();
      nPeople = 0;
      continue;
    }

    for(char c: line)
      ++Qs[c];
    nPeople++;
  }

  sumQs += Qs.size();
  for( const auto [q,n] : Qs )
    sumQs2 += n == nPeople;
  
  std::cout << sumQs << "\n";
  std::cout << sumQs2 << "\n";

}
