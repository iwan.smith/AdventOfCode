#include <stdio.h>
#include <iostream>
#include <algorithm>

int main()
{

  int validPasswords1 = 0;
  int validPasswords2 = 0;

  int min, max;
  char c;
  char password[2048];
  while ( scanf("%i-%i %c: %s\n", &min, &max, &c, password) == 4 )
  {
    std::string pword = password;

    int instances = std::count(pword.begin(), pword.end(), c);
    if ( instances >= min && instances <= max )
      ++validPasswords1;

    if ( ( pword[min-1] == c ) ^ ( pword[max-1] == c ) )
      ++validPasswords2;
  }

  std::cout << validPasswords1 << "\n";
  std::cout << validPasswords2 << "\n";


}
