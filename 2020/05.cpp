#include <iostream>
#include <algorithm>


int main()
{

  unsigned maxId = 0;
  std::string seats(1024, ' ');
  
  for(std::string line; getline(std::cin, line);)
  {

    unsigned seatId = 0;
    for(auto c: line)
    {
      seatId <<= 1;
      seatId ^=  ( c == 'B' || c == 'R' ) * 1u;
    }
    
    seats[seatId] = 'x';

    maxId = std::max(maxId, seatId);
  }
  std::cout << maxId << "\n";
  std::cout << seats.find("x x") + 1 <<"\n";


}
