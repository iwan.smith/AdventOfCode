#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <numeric>

char lookAround( int x, int y, const auto& seats, int stepx, int stepy)
{
  do
  {
    x+=stepx;
    y+=stepy;

  } while(x < seats[0].size()-1 && y < seats.size()-1 &&  x > 0 && y > 0 && seats[y][x] == '.'); 
  
  return seats[y][x];
} 


int main()
{
  
  const auto puzzleInput = std::invoke([]()
      {
        std::vector<std::string> input(std::istream_iterator<std::string>(std::cin), std::istream_iterator<std::string>());

      for( auto& line: input)
      {
        line.insert(line.begin(), '.' );
        line.insert(line.end(), '.' );
      }

      input.insert(input.begin(), std::string(input[0].size(), '.'));
      input.insert(input.end(), std::string(input[0].size(), '.'));
      return input;
    });
  //std::copy(input.begin(), input.end(), std::ostream_iterator<std::string>(std::cout, "\n"));

  { // Part1
    auto input = puzzleInput;
    for( decltype(input) newInput; newInput != input; ) 
    {
      newInput = input;
      for( int x = 1; x < input[0].size()-1; ++x)
      {
        for( int y = 1; y < input.size()-1; ++y)
        {
          if ( input[y][x] == '.' ) continue;

          unsigned sum  = (input[y-1][x-1] == '#') 
                        + (input[y-1][x  ] == '#') 
                        + (input[y-1][x+1] == '#') 
                        + (input[y  ][x-1] == '#') 
                        + (input[y  ][x+1] == '#') 
                        + (input[y+1][x-1] == '#') 
                        + (input[y+1][x  ] == '#') 
                        + (input[y+1][x+1] == '#');
          if ( input[y][x] ==  'L' && sum == 0 ) newInput[y][x] = '#';
          if ( input[y][x] ==  '#' && sum >= 4 ) newInput[y][x] = 'L';
        }
      }
      std::swap(input, newInput);
    }
    std::cout << std::accumulate(input.begin(), input.end(), 0u, [](unsigned lhs, const std::string& rhs)
        {
          return lhs + std::ranges::count(rhs, '#');
        }) << "\n";
  }

  { // Part2

    auto input = puzzleInput;
    for( decltype(input) newInput; newInput != input; ) 
    {
      newInput = input;
      for( int x = 1; x < input[0].size()-1; ++x)
      {
        for( int y = 1; y < input.size()-1; ++y)
        {
          if ( input[y][x] == '.' ) continue;

          unsigned sum = 0;
          sum += ( lookAround(x, y, input, -1, -1) == '#');
          sum += ( lookAround(x, y, input, -1,  0) == '#');
          sum += ( lookAround(x, y, input, -1,  1) == '#');

          sum += ( lookAround(x, y, input,  0, -1) == '#');
          sum += ( lookAround(x, y, input,  0,  1) == '#');

          sum += ( lookAround(x, y, input,  1, -1) == '#');
          sum += ( lookAround(x, y, input,  1,  0) == '#');
          sum += ( lookAround(x, y, input,  1,  1) == '#');
          if ( input[y][x] ==  'L' && sum == 0 ) newInput[y][x] = '#';
          if ( input[y][x] ==  '#' && sum >= 5 ) newInput[y][x] = 'L';
        }
      }
      std::swap(input, newInput);
    }
    std::cout << std::accumulate(input.begin(), input.end(), 0u, [](unsigned lhs, const std::string& rhs)
        {
          return lhs + std::ranges::count(rhs, '#');
        }) << "\n";
  }
}
