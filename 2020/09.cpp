#include <iostream>
#include <unordered_map>
#include <deque>
#include <vector>
#include <algorithm>

constexpr unsigned preambleSize = 25;

int main()
{

  std::vector<long int> numberList;

  std::deque<long int> numbers;
  std::unordered_map<long int,int> lastN;

  unsigned long invalidNumber = 0;
  for( int i; std::cin >> i; )
  {

    if ( numbers.size() >= preambleSize )
    {
      bool valid = false;
      for( auto it: numbers )
      {
        if ( lastN[i-it] )
        {
          valid = true;
          break;
        }

      }
      if (!valid) 
      {
        invalidNumber = i;
        break;
      }

      --lastN[numbers.front()];
      numbers.pop_front();
    }
  

    numbers.push_back(i);
    ++lastN[i];
    numberList.push_back(i);

  }


  std::cout << "Part1: " << invalidNumber << std::endl;
  unsigned long sum = 0;
  unsigned it2 = 0;
  for( unsigned i = 0; i < numberList.size(); ++i )
  {
    while( sum < invalidNumber )
    {
      sum += numberList[it2];
      it2++;
    }

    while( sum > invalidNumber )
    {
      it2--;
      sum -= numberList[it2];
    }

    if ( sum == invalidNumber )
    {
      auto smallest = *std::min_element(&numberList[i], &numberList[it2] );
      auto largest  = *std::max_element(&numberList[i], &numberList[it2] );
      std::cout << "Part2: " << smallest + largest << "\n";
      break;
    }
    sum -= numberList[i];

  }






}
