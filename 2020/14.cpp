#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <cmath>
#include <numeric>

auto get1bits(unsigned long i)
{

  std::vector<unsigned long> bits;
  while ( i > 0 )
  {
    auto newI = i & (i-1);
    bits.push_back(i-newI);
    i=newI;
  }
  return bits;
}


int main()
{

  unsigned long mask1 = 0;
  unsigned long mask0 = 0;
  unsigned long maskX = 0;

  std::vector<unsigned long> maskXBits;

  std::unordered_map<unsigned long, unsigned long> memMap; 
  std::unordered_map<unsigned long, unsigned long> memMap2;

  for( std::string line; std::getline(std::cin,line); )
  {
    char smask[40];
    if ( sscanf( line.data(), "mask = %s\n", smask ) == 1)
    {
      mask1 = mask0 = maskX = 0;
      for( char* c = smask; *c; ++c  )
      {
        mask1 <<= 1;
        mask0 <<= 1;
        maskX <<= 1;
        
        if ( *c == '1') mask1 += 1;
        if ( *c == '0') mask0 += 1;
        if ( *c == 'X') maskX += 1;

      }
      maskXBits = get1bits(maskX);
    }


    unsigned long memIdIn = 0;
    unsigned long valueIn = 0;
    if (sscanf( line.data(), "mem[%lu] = %lu\n", &memIdIn, &valueIn) == 2 )
    {
      //Part1
      {
        unsigned long value = valueIn;
        value = value | mask1;
        value = value & ~mask0;

        memMap[memIdIn] = value;
      }
      //Part2
      {
        unsigned long memId = memIdIn;
        memId = memId | mask1;

        for( unsigned i = 0; i < std::pow(2, maskXBits.size()); ++i )
        {
          auto memIdFloat = memId;
          for( unsigned j = 0; j < maskXBits.size(); ++j )
          {
            if ( i & (1<<j) )
              memIdFloat = memIdFloat |  maskXBits[j];
            else
              memIdFloat = memIdFloat & ~maskXBits[j];
          }
          memMap2[memIdFloat] = valueIn;
        }     
      }
    }
  }
  auto accMap = [](unsigned long acc, auto& it )
      {
        return acc + it.second;
      };

  std::cout << std::accumulate(memMap.begin(),  memMap.end(),  0ul, accMap) << "\n";
  std::cout << std::accumulate(memMap2.begin(), memMap2.end(), 0ul, accMap) << "\n";

}
