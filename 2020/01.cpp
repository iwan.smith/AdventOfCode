#include <iostream>
#include <algorithm>
#include <unordered_set>
int main(){

  std::unordered_set<int> entries;

  std::string line;

  //Part 1
  while (getline(std::cin, line) )
  {
    int e = std::stoi(line);
    entries.insert(e);
    if ( entries.contains(2020-e ) )
    {
      std::cout << "Part 1: " << e * (2020-e) << std::endl;
    }
  }

  //part 2
  for( const auto& i1: entries )
  {
    for( const auto& i2: entries )
    {
      if ( entries.contains(2020-i1-i2 ) )
      {
        std::cout << "Part 2: " << i1 * i2 * (2020-i1-i2) << std::endl;
	goto end;
      }
    }
  }
end:;
}
