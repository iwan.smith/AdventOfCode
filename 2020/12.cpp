#include <iostream>
#include <complex>
#include <cmath>
#include <numbers>

int main()
{

  // Part1
  std::complex<double> location = {0,0};
  double angle = 0;

  //Part2
  std::complex<double> location2 = {0,0};
  std::complex<double> waypoint  = {10,1};


  char direction;
  double  distance;
  while( scanf("%c%lf\n", &direction, &distance) == 2 )
  {
    constexpr std::complex<double> c_i{0,1};
    constexpr std::complex<double> c_1{1,0};
    // Part1
    switch (direction)
    {
      case 'N': location += distance * c_i; break;
      case 'E': location += distance * c_1; break;
      case 'S': location -= distance * c_i; break;
      case 'W': location -= distance * c_1; break;
      case 'L': angle -= distance; break;
      case 'R': angle += distance; break;
      case 'F': location += std::pow(c_i, -angle/90.0)*distance ; break;

    }
    //Part2
    switch (direction)
    {
      case 'N': waypoint += distance * c_i; break;
      case 'E': waypoint += distance * c_1; break;
      case 'S': waypoint -= distance * c_i; break;
      case 'W': waypoint -= distance * c_1; break;
      case 'L': waypoint *= std::pow(c_i,  distance/90.0); break;
      case 'R': waypoint *= std::pow(c_i, -distance/90.0); break;
      case 'F': location2 += distance*waypoint ; break;
    }
  }
  
  std::cout << std::abs(location.real()) + std::abs(location.imag()) << "\n";
  std::cout << std::abs(location2.real()) + std::abs(location2.imag()) << "\n";
}

