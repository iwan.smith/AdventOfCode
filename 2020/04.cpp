#include <unordered_set>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

std::pair<bool,bool> validateFields( const auto& fields )
{

  bool retPart1 = false;
  retPart1 |= ( fields.size() == 8  );
  retPart1 |= ( fields.size() == 7 && fields.contains("cid") == false  );

  if ( !retPart1 )
    return {false, false};
  
  try 
  {
    {
      std::string value = fields.at("byr");
      if ( value.size() != 4 )
        throw (std::runtime_error(std::to_string(__LINE__)));

      if ( std::stoi(value) < 1920 || std::stoi(value) > 2002 )
        throw (std::runtime_error(std::to_string(__LINE__)));
      //std::cout << value << "\n";
    }
    {
      std::string value = fields.at("iyr");
      if ( value.size() != 4 )
        throw (std::runtime_error(std::to_string(__LINE__)));

      if ( std::stoi(value) < 2010 || std::stoi(value) > 2020 )
        throw (std::runtime_error(std::to_string(__LINE__)));
      //std::cout << value << "\n";
    }
    {
      std::string value = fields.at("eyr");
      if ( value.size() != 4 )
        throw (std::runtime_error(std::to_string(__LINE__)));

      if ( std::stoi(value) < 2020 || std::stoi(value) > 2030 )
        throw (std::runtime_error(std::to_string(__LINE__)));
      //std::cout << value << "\n";
    }
    {
      std::string value = fields.at("hgt");
      
      std::string cmin(value.end()-2, value.end() );
      if ( cmin == "cm")
      {
        if ( std::stoi(value) < 150 || std::stoi(value) > 193 )
          throw (std::runtime_error(std::to_string(__LINE__)));
      }
      else if ( cmin == "in" )
      {
        if ( std::stoi(value) < 59 || std::stoi(value) > 76 )
          throw (std::runtime_error(std::to_string(__LINE__)));
      }
      else
        throw (std::runtime_error(std::to_string(__LINE__)));
        
      //std::cout << value << "\n";
    }
    {
      std::string value = fields.at("hcl");
      if ( value.size() != 7 )
        throw (std::runtime_error(std::to_string(__LINE__)));
      if ( value.at(0) != '#')
        throw (std::runtime_error(std::to_string(__LINE__)));
      if ( std::find_if(value.begin()+1, value.end(), [](char c)
          {
            return ( c < '0' || c > '9' ) && ( c < 'a' || c > 'f' ); 
          }) != value.end() )
            throw (std::runtime_error(std::to_string(__LINE__)));

      //std::cout << value << "\n";
    }
    {
      std::string value = fields.at("ecl");
      std::string colors{"amb blu brn gry grn hzl oth"};
      if (colors.find(value) == std::string::npos)
        throw (std::runtime_error(std::to_string(__LINE__)));
      //std::cout << value << "\n";
    }
    {
      std::string value = fields.at("pid");
      if ( value.size() != 9 )
        throw (std::runtime_error(std::to_string(__LINE__)));

      if ( std::find_if(value.begin(), value.end(), [](char c)
          {
            return ( c < '0' || c > '9' ); 
          }) != value.end() )
            throw (std::runtime_error(std::to_string(__LINE__)));
      //std::cout << value << "\n";
    }


  }
  catch(std::runtime_error& e)
  {
    return {retPart1, false};
  }

  return {retPart1, true};
}


int main()
{
  
  unsigned nValid1(0);
  unsigned nValid2(0);
  std::unordered_map<std::string, std::string> passportFields;
  
  
  bool valid = true;
  for(std::string line; std::getline(std::cin, line);)
  {
    if ( line.empty() )
    {
      auto [p1, p2] = validateFields( passportFields );
      nValid1 += p1;
      nValid2 += p2;
      passportFields.clear();
      continue;
    }
    
    std::stringstream ss(line);
    for(std::string field; ss >> field; )
    {
      std::string fieldName  = field.substr(0, 3);
      std::string fieldValue = field.substr(4, std::string::npos);
      passportFields.insert({fieldName, fieldValue});


    }

  } 

  auto [p1, p2] = validateFields( passportFields );
  nValid1 += p1;
  nValid2 += p2;
  
  std::cout << nValid1 << std::endl;
  std::cout << nValid2 << std::endl;
}



