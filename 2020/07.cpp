#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <regex>
#include <string>
#include <vector>

void CountContainers(auto& colorSet, const auto& bagGraph, const std::string& color)
{
  auto it = bagGraph.find(color);
  if ( it == bagGraph.end() ) 
    return;
  for (const auto& container: it->second)
  {
    const auto& [_, success] = colorSet.insert(container);
    if ( success )
      CountContainers(colorSet, bagGraph, container);
  } 
}

unsigned countContents(const auto& bagGraph, const std::string& color)
{
  
  unsigned ret = 0;

  auto it = bagGraph.find(color);
  if ( it == bagGraph.end() ) 
    return ret;
  for (const auto& [n, bagColor]: it->second)
    ret += n + n * countContents(bagGraph, bagColor);
  
  return ret;

}

int main()
{

  std::unordered_map<std::string, std::vector<std::string>> reverseBagGraph;
  std::unordered_map<std::string, std::vector<std::pair<int, std::string>>> bagGraph;


  std::regex lineDesc ("([a-z ]+) bags contain .*");
  std::regex containsDesc("([0-9]+) ([a-z ]+) bags?");
  for (std::string line; std::getline(std::cin, line); )
  {

    std::smatch m; 
    std::regex_match(line, m, lineDesc); 
    std::string bagColor = m[1].str();
    
    auto it = std::sregex_iterator(line.begin(), line.end(), containsDesc);
    for( ; it != std::sregex_iterator(); ++it )
    {
      reverseBagGraph[(*it)[2]].push_back(bagColor);
      bagGraph       [bagColor].push_back(std::make_pair(std::stoi((*it)[1]), (*it)[2]));
    }
  }

  std::unordered_set<std::string> canContainGold;
  CountContainers( canContainGold, reverseBagGraph, "shiny gold");
  std::cout << canContainGold.size() << "\n";

  std::cout << countContents  ( bagGraph, "shiny gold") << "\n";
}
