#include <iostream>
#include <vector>
#include <functional>
#include <numeric>
int main()
{
  std::string line; 
  std::getline(std::cin, line);
  
  unsigned timestamp = std::stoul(line);
  

  std::vector<std::pair<unsigned,unsigned>> busIds;
  
  {
    unsigned count = 0;  
    
    std::getline(std::cin, line);
    for( auto& c : line )
      if ( c == 'x' ) c = '0';
    for (auto it1 = line.begin(); it1 != line.end(); ++count)
    {
      auto it2 = std::find_if(it1, line.end(), [](char c) 
          { return c < '0' || c > '9';});
      std::string number{it1, it2};
      if ( number[0] != '0' )
      {
        busIds.emplace_back(std::make_pair(std::stoul(number), count));
      }
      
      it1 = std::find_if(it2, line.end(), [](char c) 
          { return c >= '0' && c <= '9';});

    }
  }

  unsigned busId(0), wait(999999999);

  for( const auto& [id, _]: busIds )
  {
    busId = id-timestamp%id < wait ?             id : busId;
    wait  = id-timestamp%id < wait ? id-timestamp%id : wait ;
  }
  
  std::cout << busId * wait << "\n";


  unsigned long lcm = busIds[0].first;
  unsigned long ts = 0;
  for( unsigned fBus = 1; fBus < busIds.size(); )
  {
    const auto& [busId, busOffset] = busIds[fBus];
    if ( (ts+busOffset)%busId == 0 )
    {
      lcm = std::lcm(lcm, busId );
      ++fBus;
    }
    else
      ts += lcm;
  }
  
  std::cout << ts << "\n";

}
